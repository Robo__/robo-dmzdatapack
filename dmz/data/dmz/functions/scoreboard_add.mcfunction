#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Scoreboard Adds for MC v1.13   |
#      :--------------------------------------------------------------:
#      |  This file is to be run once in order to set up all of the   |
#      |       scoreboard objectives that are used for items.         |
#      :--------------------------------------------------------------:
#
#
###### ITEMS - These are used to detect if an item has been used ONCE. Always reset the score after item has been used.
#
# Wooden Swords
scoreboard objectives add DMZWSwordUse minecraft.used:minecraft.wooden_sword
# Wooden Shovels
scoreboard objectives add DMZWShovelUse minecraft.used:minecraft.wooden_shovel
# Wooden Axes
scoreboard objectives add DMZWAxeUse minecraft.used:minecraft.wooden_axe
# Wooden Pickaxes
scoreboard objectives add DMZWPickaxeUse minecraft.used:minecraft.wooden_pickaxe
# Wooden Hoes
scoreboard objectives add DMZWHoeUse minecraft.used:minecraft.wooden_hoe
# Stone Swords
scoreboard objectives add DMZSSwordUse minecraft.used:minecraft.stone_sword
# Stone Shovels
scoreboard objectives add DMZSShovelUse minecraft.used:minecraft.stone_shovel
# Stone Axes
scoreboard objectives add DMZSAxeUse minecraft.used:minecraft.stone_axe
# Stone Pickaxes
scoreboard objectives add DMZSPickaxeUse minecraft.used:minecraft.stone_pickaxe
# Stone Hoes
scoreboard objectives add DMZSHoeUse minecraft.used:minecraft.stone_hoe
# Iron Swords
scoreboard objectives add DMZISwordUse minecraft.used:minecraft.iron_sword
# Iron Shovels
scoreboard objectives add DMZIShovelUse minecraft.used:minecraft.iron_shovel
# Iron Axes
scoreboard objectives add DMZIAxeUse minecraft.used:minecraft.iron_axe
# Iron Pickaxes
scoreboard objectives add DMZIPickaxeUse minecraft.used:minecraft.iron_pickaxe
# Iron Hoes
scoreboard objectives add DMZIHoeUse minecraft.used:minecraft.iron_hoe
# Golden Swords
scoreboard objectives add DMZGSwordUse minecraft.used:minecraft.golden_sword
# Golden Shovels
scoreboard objectives add DMZGShovelUse minecraft.used:minecraft.golden_shovel
# Golden Axes
scoreboard objectives add DMZGAxeUse minecraft.used:minecraft.golden_axe
# Golden Pickaxes
scoreboard objectives add DMZGPickaxeUse minecraft.used:minecraft.golden_pickaxe
# Golden Hoes
scoreboard objectives add DMZGHoeUse minecraft.used:minecraft.golden_hoe
# Diamond Swords - Infiniblade
scoreboard objectives add DMZDDSwordUse minecraft.used:minecraft.diamond_sword
# Diamond Shovels - Glass Shovel
scoreboard objectives add DMZDDShovelUse minecraft.used:minecraft.diamond_shovel
# Diamond Axes - Robax
scoreboard objectives add DMZDAxeUse minecraft.used:minecraft.diamond_axe
# Diamond Pickaxes - Gold Digger
scoreboard objectives add DMZDPickaxeUse minecraft.used:minecraft.diamond_pickaxe
# Diamond Hoes
scoreboard objectives add DMZDHoeUse minecraft.used:minecraft.diamond_hoe
# Bows - Robow, Starshot, Sparkle Fabulous
scoreboard objectives add DMZBowUse minecraft.used:minecraft.bow
# Carrot On A Stick for Wands - Cavitation, Reproduction, Restoration, Viviscection, Cmdblock Off
scoreboard objectives add DMZWandUse minecraft.used:minecraft.carrot_on_a_stick
# Kills for Curios - Experience Curio, Vampire Curio
scoreboard objectives add CurioKill totalKillCount
# Rockets - Icarian Rocket
scoreboard objectives add DMZRocketUse minecraft.used:minecraft.firework_rocket
# Wings of Icarus
scoreboard objectives add ElytraCheck dummy
# Morphtool
scoreboard objectives add Morphtool dummy
# Gold Digger counter - Counts up each time the Gold Digger pickaxe is used in order to spawn the loot bat
scoreboard objectives add GoldDigger dummy
#
#
###### MECHANICS
#
# Sneak test - used for elevators
scoreboard objectives add ElevatorSneak minecraft.custom:minecraft.sneak_time
scoreboard objectives add ElevatorTicker dummy
# Jump test - used for elevators
scoreboard objectives add ElevatorJump minecraft.custom:minecraft.jump
# Mana sytem
scoreboard objectives add HasMana dummy
scoreboard objectives add Mana dummy ▓
scoreboard objectives add ManaMax dummy ▓
scoreboard objectives add Live minecraft.custom:minecraft.time_since_death
scoreboard objectives add Hunger food
scoreboard objectives add Repl dummy
#
#
###### GENERIC
#
# Dummy trackers for use on players. If you use this please remember to reset it, or reset it at the start of your function.
scoreboard objectives add DummyTracker1 dummy
scoreboard objectives add DummyTracker2 dummy
scoreboard objectives add DummyTracker3 dummy
scoreboard objectives add DummyTracker4 dummy
# Dummy ticker for tracking things per tick. Only use for entities with tags NOT for players.
scoreboard objectives add Ticker1 dummy
scoreboard objectives add Ticker2 dummy
scoreboard objectives add Ticker3 dummy
scoreboard objectives add Ticker4 dummy
# Game scoreboard dummy objectives.
scoreboard objectives add DummyVar1 dummy
scoreboard objectives add DummyVar2 dummy
scoreboard objectives add DummyVar3 dummy
scoreboard objectives add DummyVar4 dummy
# Game Kills
scoreboard objectives add VariableKill totalKillCount
scoreboard objectives add VariablePKill playerKillCount
scoreboard objectives add TeamKillAqua teamkill.aqua Kills By Aqua
scoreboard objectives add TeamKillBlack teamkill.black Kills By Black
scoreboard objectives add TeamKillBlue teamkill.blue Kills By Blue
scoreboard objectives add TeamKillDAqua teamkill.dark_aqua Kills By Dark Aqua
scoreboard objectives add TeamKillDBlue teamkill.dark_blue Kills By Dark Blue
scoreboard objectives add TeamKillDGray teamkill.dark_gray Kills By Dark Gray
scoreboard objectives add TeamKillDGreen teamkill.dark_green Kills By Dark Green
scoreboard objectives add TeamKillDPurple teamkill.dark_purple Kills By Dark Purple
scoreboard objectives add TeamKillDRed teamkill.dark_red Kills By Dark Red
scoreboard objectives add TeamKillGold teamkill.gold Kills By Gold
scoreboard objectives add TeamKillGray teamkill.gray Kills By Gray
scoreboard objectives add TeamKillGreen teamkill.green Kills By Green
scoreboard objectives add TeamKillLPurple teamkill.light_purple Kills By Light Purple
scoreboard objectives add TeamKillRed teamkill.red Kills By Red
scoreboard objectives add TeamKillWhite teamkill.white Kills By White
scoreboard objectives add TeamKillYellow teamkill.yellow Kills By Yellow
# Game Deaths
scoreboard objectives add VariableDeath deathCount
scoreboard objectives add KilledByAqua killedByTeam.aqua Killed By Aqua
scoreboard objectives add KilledByBlack killedByTeam.black Killed By Black
scoreboard objectives add KilledByBlue killedByTeam.blue Killed By Blue
scoreboard objectives add KilledByDAqua killedByTeam.dark_aqua Killed By Dark Aqua
scoreboard objectives add KilledByDBlue killedByTeam.dark_blue Killed By Dark Blue
scoreboard objectives add KilledByDGray killedByTeam.dark_gray Killed By Dark Gray
scoreboard objectives add KilledByDGreen killedByTeam.dark_green Killed By Dark Green
scoreboard objectives add KilledByDPurple killedByTeam.dark_purple Killed By Dark Purple
scoreboard objectives add KilledByDRed killedByTeam.dark_red Killed By Dark Red
scoreboard objectives add KilledByGold killedByTeam.gold Killed By Gold
scoreboard objectives add KilledByGray killedByTeam.gray Killed By Gray
scoreboard objectives add KilledByGreen killedByTeam.green Killed By Green
scoreboard objectives add KilledByLPurple killedByTeam.light_purple Killed By Light Purple
scoreboard objectives add KilledByRed killedByTeam.red Killed By Red
scoreboard objectives add KilledByWhite killedByTeam.white Killed By White
scoreboard objectives add KilledByYellow killedByTeam.yellow Killed By Yellow
