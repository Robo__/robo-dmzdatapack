
give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GRedstone:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Redstone Ready' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GSlime:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Slimed' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GFire:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Inferno' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GSmoke:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Smoke Screen' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GEnd:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Extraplanar' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GBubble:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Bubbler' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GCrit:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Critical' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GArrow:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Deadshot' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GParty:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Confetti' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GHeaven:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Heavenly' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GVapor:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Vapor' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GAsh:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Ash' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GMoney:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Midas Touch' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GMoney1:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Diamond Encrusted' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GMoney2:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Emerald Infused' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GMoney3:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Iron Curtain' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GMoney4:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Lapis Love' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GArcane:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Arcane' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GFizzle:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Fizzy' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GDragon:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Dragon Energy' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}

give @p minecraft:knowledge_book{GlimmerItem:1b,ApplyGlimmer:1b,GNautilus:1b,display:{Name:"{\"text\":\"Glimmer\",\"color\":\"yellow\",\"italic\":false}",Lore:["When dropped on armor:","Applies 'Conduit Covered' effect"]},HideFlags:1,Enchantments:[{id:"minecraft:protection",lvl:1}]}