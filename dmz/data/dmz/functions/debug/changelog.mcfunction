tellraw @s {"text":" --- V1 --- ","color":"gold"}
tellraw @s {"text":" - Added 6 new wands ","color":"gold"}
tellraw @s {"text":" - Added gauntlets for every material type ","color":"gold"}
tellraw @s {"text":" - Added 19 new recipes ","color":"gold"}
tellraw @s {"text":" --- V2 --- ","color":"gold"}
tellraw @s {"text":" - Wands now run off of Mana instead of experience ","color":"gold"}
tellraw @s {"text":" - All wand have been resprited ","color":"gold"}
tellraw @s {"text":" - Minor fixes ","color":"gold"}
tellraw @s {"text":" - Added tag documentation ","color":"gold"}
tellraw @s {"text":" --- V3 --- ","color":"gold"}
tellraw @s {"text":" - 2 new wands ","color":"gold"}
tellraw @s {"text":" - All wands now have descriptions ","color":"gold"}
tellraw @s {"text":" - 1 new recipe ","color":"gold"}
tellraw @s {"text":" - bracelets have been added ","color":"gold"}
tellraw @s {"text":" - Wickaxe has been removed ","color":"gold"}
tellraw @s {"text":" - Minor fixes ","color":"gold"}
tellraw @s {"text":" --- V4 --- ","color":"gold"}
tellraw @s {"text":" - 2 new armor sets ","color":"gold"}
tellraw @s {"text":" - Changed how Mana and bracelets function ","color":"gold"}
tellraw @s {"text":" - Tweaked Mana Replenish times ","color":"gold"}
tellraw @s {"text":" --- V5 --- ","color":"gold"}
tellraw @s {"text":" - Added glimmers ","color":"gold"}
tellraw @s {"text":" - added 'listglimmertags' to documentation ","color":"gold"}
tellraw @s {"text":" - wand projectiles are now anchored to the eye position ","color":"gold"}
tellraw @s {"text":" - Minor fixes ","color":"gold"}
tellraw @s {"text":" --- V6 --- ","color":"gold"}
tellraw @s {"text":" - Added placables ","color":"gold"}
tellraw @s {"text":" - Added 'giveblocks' to documentation ","color":"gold"}
tellraw @s {"text":" - Minor fixes ","color":"gold"}


