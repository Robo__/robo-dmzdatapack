
tellraw @s {"text":" ---Tag--------Name--------Desc--- ","color":"gold"}
tellraw @s {"text":" 'GRedstone'('Redstone Ready') displays redstone particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GSlime'('Slimed') displays slime particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GFire'('Inferno') displays flame particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GSmoke'('Smoke Screen') displays smoke particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GEnd'('Extraplanar') displays enderman/nether portal particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GBubble'('Bubbler') displays splash particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GCrit'('Critical') displays enchanted weapon hit particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GArrow'('Deadshot') displays critical hit particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GParty'('Confetti') displays colored splash potion particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GHeaven'('Heavenly') displays white instant splash potion particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GVapor'('Vapor') displays underwater wake particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GAsh'('Ash') displays mycelium particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GMoney'('Midas Touch') displays golden block particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GMoney1'('Diamond Encrusted') displays diamond block particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GMoney2'('Emerald Infused') displays emerald block particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GMoney3'('Iron Curtain') displays iron block particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GMoney4'('Lapis Love') displays lapis block particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GArcane'('Arcane') displays enchanting particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GFizzle'('Fizzy') displays fishing wake particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GDragon'('Dragon Energy') displays dragon breath particles","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GNautilus'('Conduit Covered') displays conduit ambient particles","color":"gold"}