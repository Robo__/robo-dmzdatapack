tellraw @s {"text":" ---Tag--------Desc--- ","color":"gold"}
tellraw @s {"text":" 'Leatherb' +1 Mana regeneration when held in off/main hand","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Chainb' +2 Mana regeneration when held in off/main hand","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Ironb' +3 Mana regeneration when held in off/main hand","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Goldb' +4 Mana regeneration when held in off/main hand","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Diamondb' +5 Mana regeneration when held in off/main hand","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'BulletWand' run Damage Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'HealthWand' run Health Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'FireWand' run Fire Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'WaterWand' run Ice Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'ElecWand' run Lightning Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'VoidWand' run Darkness Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'TimeWand' run Time Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'WeatherWand' run Weather Wand function on use","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'ApplyGlimmer' used by glimmer books to apply glimmers","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'HasGlimmer' used by glimmer books to stop glimmers from being applied","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'GlimmerItem' used by glimmer books to detect items that have glimmer effects","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'CustomItem' used for all armor and weapons to denote that it should run a check to run a function","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Placed' used by placables to detect when they're first placed","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'CustomBlock' used by placables to determine what happens to their entities","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Solid' used by placables to determine if it's solid or not","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Container' used by placables to determine if it's a container or not","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Glowing' used by placables to determine if it's a glowing block","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'CanSit' used by placables to determine if it can be sat in","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'HoldItem' used by placables to determine it has table functionality","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'Statue' used by placables to determine it has statue functionality","color":"gold"}
tellraw @s {"text":" ----- ","color":"gold"}
tellraw @s {"text":" 'OneHand' used by items to determine if it cannot be duel weld","color":"gold"}
