tellraw @s {"text":"Features:","color":"dark_aqua"}
tellraw @s {"text":"--------------------","color":"dark_aqua"}
tellraw @s {"text":"Custom Items:","color":"aqua"}
tellraw @s {"text":" -Glass Shovel","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: dotlizard"}}
tellraw @s {"text":" -Glimmer Books","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Graceless Talaria","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: k0nker"}}
tellraw @s {"text":" -Infiniblade","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Morphtool","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: derborgus3333"}}
tellraw @s {"text":" -Xmist swords","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: sawarahh"}}
tellraw @s {"text":" -Rain Bow","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Robax","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Robow","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Scholar's Curio","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: derborgus3333"}}
tellraw @s {"text":" -Starshot","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Torchwood","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: derborgus3333"}}
tellraw @s {"text":" -Vampire's Curio","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: derborgus3333"}}
tellraw @s {"text":" -Wands","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01, derborgus3333, k0nker"}}
tellraw @s {"text":" -Wings of Icarus","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: k0nker"}}
tellraw @s {"text":"--------------------","color":"dark_aqua"}
tellraw @s {"text":"Mechanics:","color":"aqua"}
tellraw @s {"text":" -Lapis Elevators","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: k0nker"}}
tellraw @s {"text":" -Mana","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
tellraw @s {"text":" -Glimmers","color":"gold","hoverEvent":{"action":"show_text","value":"Created by: Robo_01"}}
