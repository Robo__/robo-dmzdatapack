

execute as @s[tag=!Placed] at @s run execute as @e[type=item,limit=1,distance=0..0.75,nbt=!{NoGravity:1b},nbt={Invulnerable:1b}] at @s run playsound item.armor.equip_generic master @a[distance=1..5] ~ ~ ~ 0.8 1 0.8

execute as @s[tag=!Placed] at @s run data merge entity @e[type=item,limit=1,distance=0..0.75,nbt=!{NoGravity:1b}] {NoGravity:1b,Age:-32768,Health:999999,Motion:[0.0,0.0,0.0]}

execute as @s[tag=!Placed] at @s run data merge entity @e[type=item,limit=1,distance=0..1.25] {Invulnerable:1b}

execute as @s[tag=!Placed] at @s run teleport @e[type=item,limit=1,distance=0..0.75] @s

execute as @s[tag=!Placed] at @s run teleport @e[type=item,limit=1,distance=0..0.75] ~ ~1 ~
