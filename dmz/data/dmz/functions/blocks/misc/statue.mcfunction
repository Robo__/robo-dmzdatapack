
execute as @s[y_rotation=45] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[45.0F,0.0F]}

execute as @s[y_rotation=90] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[90.0F,0.0F]}

execute as @s[y_rotation=135] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[135.0F,0.0F]}

execute as @s[y_rotation=180] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[180.0F,0.0F]}

execute as @s[y_rotation=225] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[225.0F,0.0F]}

execute as @s[y_rotation=270] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[270.0F,0.0F]}

execute as @s[y_rotation=315] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[315.0F,0.0F]}

execute as @s[y_rotation=0] at @s run summon armor_stand ~ ~0.025 ~ {NoGravity:1b,Invulnerable:1b,ShowArms:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["InStatue"],Pose:{Body:[0.0f,0.0f,0.0f],LeftArm:[0.0f,0.0f,0.0f],RightArm:[0.0f,0.0f,0.0f],LeftLeg:[0.0f,0.0f,0.0f],RightLeg:[0.0f,0.0f,0.0f],Head:[0.0f,0.0f,0.0f]},Rotation:[0.0F,0.0F]}

