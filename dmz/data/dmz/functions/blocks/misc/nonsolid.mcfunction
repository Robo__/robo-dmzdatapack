

execute as @s at @s unless block ~ ~ ~ structure_void run setblock ~ ~ ~ minecraft:structure_void replace

execute as @s[tag=!CanSit] at @s if block ~ ~ ~ structure_void run summon pig ~ ~ ~ {Silent:1b,OnGround:0b,NoGravity:1b,Invulnerable:1b,DeathLootTable:"null",PersistenceRequired:1b,NoAI:1b,CanPickUpLoot:0b,Health:1f,InLove:0,Age:-100,ForcedAge:-100,LoveCauseLeast:0,LoveCauseMost:0,EatingHaystack:0b,Tame:1b,Tags:["Collision"],Attributes:[{Name:generic.maxHealth,Base:1},{Name:generic.followRange,Base:0},{Name:generic.knockbackResistance,Base:1},{Name:generic.movementSpeed,Base:0},{Name:generic.attackDamage,Base:0},{Name:generic.armor,Base:0},{Name:generic.armorToughness,Base:0},{Name:pig.jumpStrength,Base:0}],Saddle:0,ActiveEffects:[{Id:14b,Amplifier:10b,Duration:999999,ShowParticles:0b}]}

execute as @s[tag=CanSit] at @s if block ~ ~ ~ structure_void run summon pig ~ ~ ~ {Silent:1b,OnGround:0b,NoGravity:1b,Invulnerable:1b,DeathLootTable:"null",PersistenceRequired:1b,NoAI:1b,CanPickUpLoot:0b,Health:1f,InLove:0,Age:-100,ForcedAge:-100,LoveCauseLeast:0,LoveCauseMost:0,EatingHaystack:0b,Tame:1b,Tags:["Collision"],Attributes:[{Name:generic.maxHealth,Base:1},{Name:generic.followRange,Base:0},{Name:generic.knockbackResistance,Base:1},{Name:generic.movementSpeed,Base:0},{Name:generic.attackDamage,Base:0},{Name:generic.armor,Base:0},{Name:generic.armorToughness,Base:0},{Name:pig.jumpStrength,Base:0}],Saddle:1,ActiveEffects:[{Id:14b,Amplifier:10b,Duration:999999,ShowParticles:0b}]}





