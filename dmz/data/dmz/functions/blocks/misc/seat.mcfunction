
#Seat Management

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=CanSit] at @s unless block ~ ~ ~ air run summon pig ~ ~ ~ {Silent:1b,OnGround:0b,NoGravity:1b,Invulnerable:1b,DeathLootTable:"null",PersistenceRequired:1b,NoAI:1b,CanPickUpLoot:0b,Health:1f,InLove:0,Age:-100,ForcedAge:-100,LoveCauseLeast:0,LoveCauseMost:0,EatingHaystack:0b,Tame:1b,Tags:["Seat"],Attributes:[{Name:generic.maxHealth,Base:1},{Name:generic.followRange,Base:0},{Name:generic.knockbackResistance,Base:1},{Name:generic.movementSpeed,Base:0},{Name:generic.attackDamage,Base:0},{Name:generic.armor,Base:0},{Name:generic.armorToughness,Base:0},{Name:pig.jumpStrength,Base:0}],Saddle:1,ActiveEffects:[{Id:14b,Amplifier:10b,Duration:9,ShowParticles:0b}]}

execute as @e[type=armor_stand,tag=CustomBlock,tag=CanSit,tag=Placed] at @s run teleport @e[tag=Seat,type=pig,distance=0..0.5] @s

execute as @e[type=armor_stand,tag=CustomBlock,tag=CanSit,tag=Placed] at @s run teleport @e[tag=Seat,type=pig,distance=0..0.5] ~ ~0.04 ~

execute as @e[tag=Seat,type=pig] at @s run data merge entity @s {ForcedAge:-500}

execute as @e[tag=Seat,type=pig] at @s run data merge entity @s {Age:-500}



#Collision

execute as @s at @s unless block ~ ~ ~ air run summon pig ~ ~ ~ {Silent:1b,OnGround:0b,NoGravity:1b,Invulnerable:1b,DeathLootTable:"null",PersistenceRequired:1b,NoAI:1b,CanPickUpLoot:0b,Health:1f,InLove:0,Age:-100,ForcedAge:-100,LoveCauseLeast:0,LoveCauseMost:0,EatingHaystack:0b,Tame:1b,Tags:["Seat"],Attributes:[{Name:generic.maxHealth,Base:1},{Name:generic.followRange,Base:0},{Name:generic.knockbackResistance,Base:1},{Name:generic.movementSpeed,Base:0},{Name:generic.attackDamage,Base:0},{Name:generic.armor,Base:0},{Name:generic.armorToughness,Base:0},{Name:pig.jumpStrength,Base:0}],Saddle:0,ActiveEffects:[{Id:14b,Amplifier:10b,Duration:999999,ShowParticles:0b}]}


#Holding items

execute as @s[tag=HoldItem,tag=!Placed] at @s run execute as @e[type=item,limit=1,distance=0..0.75,nbt=!{NoGravity:1b},nbt={Invulnerable:1b}] at @s run playsound item.armor.equip_generic master @a[distance=1..5] ~ ~ ~ 0.8 1 0.8

execute as @s[tag=HoldItem,tag=!Placed] at @s run data merge entity @e[type=item,limit=1,distance=0..0.75,nbt=!{NoGravity:1b}] {NoGravity:1b,Age:-32768,Health:999999,Motion:[0.0,0.0,0.0]}

execute as @s[tag=HoldItem,tag=!Placed] at @s run data merge entity @e[type=item,limit=1,distance=0..1.25] {Invulnerable:1b}

execute as @s[tag=HoldItem,tag=!Placed] at @s run teleport @e[type=item,limit=1,distance=0..0.75] @s

execute as @s[tag=HoldItem,tag=!Placed] at @s run teleport @e[type=item,limit=1,distance=0..0.75] ~ ~1 ~