

#check for more specific requests

execute as @s[name=Crate] at @s unless block ~ ~ ~ chest run setblock ~ ~ ~ minecraft:chest[type=left]{CustomName:"{\"text\":\"Wooden Crate\"}"} replace

execute as @s[name=IronCrate] at @s unless block ~ ~ ~ chest run setblock ~ ~ ~ minecraft:chest[type=left]{CustomName:"{\"text\":\"Iron Crate\"}"} replace


#generic container is placed first (in the case of mix&matching)

execute as @s at @s unless block ~ ~ ~ chest run setblock ~ ~ ~ minecraft:chest[type=left]{CustomName:"{\"text\":\"Container\"}"} replace


