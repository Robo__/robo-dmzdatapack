






#Chair Rotations

execute as @s[name=Chair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:2} 1

execute as @s[name=Chair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:2} 1

execute as @s[name=Chair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:2} 1

execute as @s[name=Chair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:2} 1

#Iron Chair Rotations

execute as @s[name=IronChair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:10} 1

execute as @s[name=IronChair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:10} 1

execute as @s[name=IronChair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:10} 1

execute as @s[name=IronChair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:10} 1

#Spruce Chair Rotations

execute as @s[name=SpruceChair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:13} 1

execute as @s[name=SpruceChair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:13} 1

execute as @s[name=SpruceChair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:13} 1

execute as @s[name=SpruceChair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:13} 1

#Birch Chair Rotations

execute as @s[name=BirchChair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:16} 1

execute as @s[name=BirchChair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:16} 1

execute as @s[name=BirchChair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:16} 1

execute as @s[name=BirchChair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:16} 1

#Jungle Chair Rotations

execute as @s[name=JungleChair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:19} 1

execute as @s[name=JungleChair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:19} 1

execute as @s[name=JungleChair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:19} 1

execute as @s[name=JungleChair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:19} 1

#Acacia Chair Rotations

execute as @s[name=AcaciaChair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:22} 1

execute as @s[name=AcaciaChair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:22} 1

execute as @s[name=AcaciaChair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:22} 1

execute as @s[name=AcaciaChair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:22} 1

#Darkoak Chair Rotations

execute as @s[name=DarkChair,y_rotation=45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:25} 1

execute as @s[name=DarkChair,y_rotation=-45] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:25} 1

execute as @s[name=DarkChair,y_rotation=135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:25} 1

execute as @s[name=DarkChair,y_rotation=-135] at @s run replaceitem entity @s armor.head minecraft:diamond_hoe{Unbreakable:1b,Damage:25} 1






