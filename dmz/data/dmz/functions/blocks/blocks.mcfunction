#containers

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=Solid,tag=Container,tag=!Glowing] at @s unless block ~ ~ ~ chest run function dmz:blocks/misc/containers

#lamps

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=Solid,tag=!Container,tag=Glowing] at @s unless block ~ ~ ~ glowstone run function dmz:blocks/misc/glowing

#solid

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=Solid,tag=!Container,tag=!Glowing] at @s unless block ~ ~ ~ barrier run function dmz:blocks/misc/solid

#nonsolid

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=!Solid,tag=!Container,tag=!Glowing] at @s unless block ~ ~ ~ structure_void run function dmz:blocks/misc/nonsolid

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=!Solid,tag=!Container,tag=!Glowing] at @s run teleport @e[tag=Collision,type=pig,distance=0..0.5] @s

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed,tag=!Solid,tag=!Container,tag=!Glowing] at @s run teleport @e[tag=Collision,type=pig,distance=0..0.5] ~ ~0.04 ~

#statue

execute as @e[type=armor_stand,tag=CustomBlock,tag=Statue,tag=Placed] at @s run function dmz:blocks/misc/statue

#chair rotations

execute as @e[type=armor_stand,tag=CustomBlock,tag=Placed] at @s run function dmz:blocks/misc/chairs

#hold item

execute as @e[type=armor_stand,tag=CustomBlock,tag=!Placed,tag=HoldItem] at @s run function dmz:blocks/misc/tables

#drop item

execute as @e[type=armor_stand,tag=CustomBlock,tag=!Placed,tag=!Solid] at @s unless block ~ ~ ~ structure_void run function dmz:blocks/break


tag @e[tag=Placed] remove Placed

execute as @e[tag=!Placed,tag=CustomBlock] at @s run function dmz:blocks/kill



#kill out of place entities

execute as @e[tag=Collision,type=pig] at @s unless block ~ ~ ~ structure_void run teleport @s ~ ~-100000 ~

execute as @e[tag=InStatue,type=armor_stand] at @s unless block ~ ~ ~ barrier run kill @s



#keep collision entities hidden and permanently small

effect give @e[tag=Collision,type=pig,nbt=!{ActiveEffects:[{Id:11b,Duration:40}]}] resistance 999999 10 true

effect give @e[tag=Collision,type=pig,nbt=!{ActiveEffects:[{Id:14b,Duration:40}]}] invisibility 999999 10 true

execute as @e[tag=Collision,type=pig] at @s run data merge entity @s {ForcedAge:-500}

execute as @e[tag=Collision,type=pig] at @s run data merge entity @s {Age:-500}