#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |     DMZCraft MCFuntion file that is executed every tick      |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
##### Custom item checks
# Check if item is being held by player then run file to check which function to load
execute as @a[nbt={Inventory:[{Slot:-106b,tag:{CustomItem:1b}}]}] run function dmz:items/items
execute as @a[nbt={SelectedItem:{tag:{CustomItem:1b}}}] run function dmz:items/items
# Armor slots
# Helm
# Chest
# Leggings
# Boots
execute as @a[nbt={Inventory:[{Slot:100b,tag:{CustomBoots:1b}}]}] run function dmz:items/armor/boots/_boots
# Armor Sets - Whole set must be worn
execute as @a[nbt={Inventory:[{Slot:100b,tag:{CustomItem:1b}}]},nbt={Inventory:[{Slot:101b,tag:{CustomItem:1b}}]},nbt={Inventory:[{Slot:102b,tag:{CustomItem:1b}}]},nbt={Inventory:[{Slot:103b,tag:{CustomItem:1b}}]}] run function dmz:items/armor/sets/_sets
# Miscellaneous - elytra, curios, etc.
execute as @a[nbt={Inventory:[{Slot:102b,tag:{CustomMisc:1b}}]}] run function dmz:items/misc/_misc
#
# Check for entity spawned by custom items
execute as @e[type=armor_stand,tag=CItemEntity] run function dmz:items/item_entity
execute as @e[type=arrow,tag=CItemEntity] run function dmz:items/item_entity
#
# Morphtool - A tool that changes type every time you drop it. Must run every tick
execute if entity @e[nbt={Item:{tag:{Morphtool:1b}}},type=minecraft:item] run function dmz:items/misc/morphtool
#execute run function dmz:items/misc/morphtool
execute as @e[tag=CItemEntity] run function dmz:items/item_entity
#
#
##### Reset used scores
# Curio score resets - Must be AFTER all Curios
execute as @a[scores={CurioKill=1..}] run scoreboard players reset @s CurioKill
# Generic
execute as @a[scores={DMZBowUse=1..}] run scoreboard players reset @s DMZBowUse
execute as @a[scores={DMZWandUse=1..}] run scoreboard players reset @s DMZWandUse
execute as @a[scores={DMZRocketUse=1..}] run scoreboard players reset @s DMZRocketUse
# Wooden
execute as @a[scores={DMZWSwordUse=1..}] run scoreboard players reset @s DMZWSwordUse
execute as @a[scores={DMZWShovelUse=1..}] run scoreboard players reset @s DMZWShovelUse
execute as @a[scores={DMZWAxeUse=1..}] run scoreboard players reset @s DMZWAxeUse
execute as @a[scores={DMZWPickaxeUse=1..}] run scoreboard players reset @s DMZWPickaxeUse
execute as @a[scores={DMZWHoeUse=1..}] run scoreboard players reset @s DMZWHoeUse
# Iron
execute as @a[scores={DMZISwordUse=1..}] run scoreboard players reset @s DMZISwordUse
execute as @a[scores={DMZIShovelUse=1..}] run scoreboard players reset @s DMZIShovelUse
execute as @a[scores={DMZIAxeUse=1..}] run scoreboard players reset @s DMZIAxeUse
execute as @a[scores={DMZIPickaxeUse=1..}] run scoreboard players reset @s DMZIPickaxeUse
execute as @a[scores={DMZIHoeUse=1..}] run scoreboard players reset @s DMZIHoeUse
# Golden
execute as @a[scores={DMZGSwordUse=1..}] run scoreboard players reset @s DMZGSwordUse
execute as @a[scores={DMZGShovelUse=1..}] run scoreboard players reset @s DMZGShovelUse
execute as @a[scores={DMZGAxeUse=1..}] run scoreboard players reset @s DMZGAxeUse
execute as @a[scores={DMZGPickaxeUse=1..}] run scoreboard players reset @s DMZGPickaxeUse
execute as @a[scores={DMZGHoeUse=1..}] run scoreboard players reset @s DMZGHoeUse
# Diamond
execute as @a[scores={DMZDSwordUse=1..}] run scoreboard players reset @s DMZDSwordUse
execute as @a[scores={DMZDShovelUse=1..}] run scoreboard players reset @s DMZDShovelUse
execute as @a[scores={DMZDAxeUse=1..}] run scoreboard players reset @s DMZDAxeUse
execute as @a[scores={DMZDPickaxeUse=1..}] run scoreboard players reset @s DMZDPickaxeUse
execute as @a[scores={DMZDHoeUse=1..}] run scoreboard players reset @s DMZDHoeUse
