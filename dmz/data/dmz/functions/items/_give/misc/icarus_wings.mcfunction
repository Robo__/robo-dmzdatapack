#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wings of Icarus - Elytra that give the player a weak rocket after 220 ticks, and a strong rocket after 1220. Timer resets when rocket is used.
# Give command:
give @p minecraft:elytra{display:{Name:"{\"text\":\"Wings of Icarus\",\"color\":\"gold\",\"italic\":false}",Lore:["Rockets keep falling out of these wings!","They also seem to eat the same rockets."]},HideFlags:7,CustomMisc:1b,IcarusWings:1b,Damage:2,Unbreakable:1b,CustomItem:1b,Enchantments:[{id:"minecraft:feather_falling",lvl:2}]} 1
