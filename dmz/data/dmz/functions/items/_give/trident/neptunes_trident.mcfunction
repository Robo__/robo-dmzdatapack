#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# DOES NOT WORK YET
# Neptune's Trident - Acts like an enderpearl, but works underwater!
# Give command:
give @p minecraft:trident{display:{Name:"{\"text\":\"Neptune's Trident\",\"color\":\"dark_aqua\",\"italic\":false}",Lore:["When lost at sea, pray to Neptune... right?"]},HideFlags:5,CustomTrident:1b,NeptuneTrident:1b,HideFlags:1,Unbreakable:1b,CustomItem:1b,Enchantments:[{id:"minecraft:loyalty",lvl:3},{id:"minecraft:impaling",lvl:5},{id:"minecraft:channeling",lvl:1}]} 1
