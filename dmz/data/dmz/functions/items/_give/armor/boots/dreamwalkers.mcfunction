#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Dreamwalkers - MOD ONLY - So you don't die while testing in survival mode.
# Give command:
give @p minecraft:chainmail_boots{display:{Name:"{\"text\":\"Dreamwalkers\",\"color\":\"dark_purple\",\"italic\":false}",Lore:["In dreams you're invincible."]},HideFlags:7,CustomBoots:1b,Dreamwalkers:1b,Unbreakable:1b,CustomItem:1b,Enchantments:[{id:"minecraft:protection",lvl:10},{id:"minecraft:fire_protection",lvl:10},{id:"minecraft:feather_falling",lvl:10},{id:"minecraft:blast_protection",lvl:10},{id:"minecraft:projectile_protection",lvl:10},{id:"minecraft:respiration",lvl:10},{id:"minecraft:thorns",lvl:10},{id:"minecraft:depth_strider",lvl:10},{id:"minecraft:frost_walker",lvl:1}],AttributeModifiers:[{AttributeName:"generic.maxHealth",Name:"generic.maxHealth",Amount:30,Operation:0,UUIDLeast:292106,UUIDMost:202853,Slot:"feet"},{AttributeName:"generic.knockbackResistance",Name:"generic.knockbackResistance",Amount:10,Operation:0,UUIDLeast:792487,UUIDMost:253659,Slot:"feet"},{AttributeName:"generic.attackDamage",Name:"generic.attackDamage",Amount:20,Operation:0,UUIDLeast:697202,UUIDMost:613096,Slot:"feet"},{AttributeName:"generic.armor",Name:"generic.armor",Amount:50,Operation:0,UUIDLeast:384398,UUIDMost:563135,Slot:"feet"},{AttributeName:"generic.armorToughness",Name:"generic.armorToughness",Amount:30,Operation:0,UUIDLeast:978186,UUIDMost:874231,Slot:"feet"},{AttributeName:"generic.luck",Name:"generic.luck",Amount:20,Operation:0,UUIDLeast:315500,UUIDMost:319112,Slot:"feet"}]} 1
