#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |      DMZCraft MCFuntion file that for custom pickaxes        |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
#
# Torchwood - Gives the player night vision when held in main or off hand.
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Torchwood:1b}}]}] run function dmz:items/pickaxe/torchwood
execute as @s[nbt={SelectedItem:{tag:{Torchwood:1b}}}] run function dmz:items/pickaxe/torchwood
#
# Gold Digger - summons custom loot bat that dies with mineshaft chest loot after 300 digs with pick.
execute as @s[scores={DMZDPickaxeUse=1..},nbt={SelectedItem:{tag:{GoldDigger:1b}}}] run function dmz:items/pickaxe/gold_digger
