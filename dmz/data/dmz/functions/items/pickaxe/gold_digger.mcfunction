#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Gold Digger - Spawns bat with custom loot that dies instantly after so many digs
scoreboard players add @s[scores={DMZDPickaxeUse=1}] GoldDigger 1
execute as @s[scores={GoldDigger=300..}] at @s run summon bat ~ ~ ~ {Attributes:[{Name:generic.maxHealth,Base:1}],Health:1.0f,NoAI:1,Silent:1,DeathLootTable:"minecraft:chests/simple_dungeon",ActiveEffects:[{Id:14,Amplifier:0,Duration:200,ShowParticles:0b},{Id:20,Amplifier:10,Duration:200,ShowParticles:0b}]}
execute as @s[scores={GoldDigger=300..}] at @s run playsound minecraft:block.enchantment_table.use master @a ~ ~ ~ 1 1 0
execute as @s[scores={GoldDigger=300..}] at @s run particle minecraft:totem_of_undying ~ ~1 ~ 0.5 0.5 0.5 0 10 normal @a
scoreboard players reset @s[scores={GoldDigger=300..}] GoldDigger
scoreboard players reset @s[scores={DMZDPickaxeUse=1..}] DMZDPickaxeUse
