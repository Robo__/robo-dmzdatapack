#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Robax - Automatically turns logs into coal as well as catches bad guys on fire.
execute as @s[scores={DMZDAxeUse=1}] at @s run playsound minecraft:entity.generic.burn master @a[distance=..15] ~ ~ ~ 0.6 1.2 0.5
execute as @s[scores={DMZDAxeUse=1}] at @s as @e[type=!item,distance=0..6] run data merge entity @s {Fire:40}
execute as @s[scores={DMZDAxeUse=1}] at @s anchored eyes run particle minecraft:dust 255 0 0 1 ^ ^ ^2 1 1 1 0 20 normal @s
execute as @s[scores={DMZDAxeUse=1}] at @s anchored eyes run particle minecraft:lava ^ ^ ^2 1 1 1 0.1 5 normal @s
execute as @s[scores={DMZDAxeUse=1}] at @s run playsound fire.ignite master @a[distance=..20] ~ ~ ~ 1.2 1 0.5
execute as @s[scores={DMZDAxeUse=1}] at @s as @e[type=!item,distance=0..3] run data merge entity @s {Fire:80}
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:oak_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:spruce_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:birch_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:acacia_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:dark_oak_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:jungle_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:stripped_oak_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:stripped_spruce_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:stripped_birch_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:stripped_acacia_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:stripped_dark_oak_log"}},distance=..10] add RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:stripped_jungle_log"}},distance=..10] add RobaxDrop
execute as @e[type=item,tag=RobaxDrop] run data merge entity @s {Item:{id:"minecraft:coal_block",Count:1b}}
tag @e[type=item,tag=RobaxDrop] remove RobaxDrop
execute as @s[scores={DMZDAxeUse=1}] at @s run effect give @e[type=!item_frame,type=!player,distance=0..5] minecraft:glowing 5 1 false
scoreboard players reset @s[scores={DMZDAxeUse=1..}] DMZDAxeUse
