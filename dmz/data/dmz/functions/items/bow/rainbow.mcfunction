#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Rain Bow - Shoots a rainbow of colors restoring health and hunger to those hit.
execute as @s[scores={DMZBowUse=1}] at @s as @e[type=arrow,distance=..2] run data merge entity @s {Tags:["RainBowArrow","CItemEntity"],pickup:0,CustomPotionEffects:[{Id:6,Amplifier:1,Duration:3},{Id:9,Amplifier:1,Duration:140},{Id:10,Amplifier:1,Duration:0},{Id:23,Amplifier:1,Duration:20},{Id:26,Amplifier:1,Duration:0}],damage:0.1}
execute as @s[scores={DMZBowUse=1}] at @s run playsound random.successful_hit master @a[distance=..20] ~ ~ ~ 0.5 0.5 0.5
execute as @s[scores={DMZBowUse=1}] at @s run playsound note.harp master @a[distance=..20] ~ ~ ~ 1 0.5 1
execute as @s[scores={DMZBowUse=1}] at @s run particle minecraft:dust 25 30 30 1 ~ ~1 ~ 0.7 0.5 0.7 0.5 11 normal
execute as @s[tag=RainBowArrow,type=arrow,nbt={inGround:0b},scores={Ticker1=1}] at @s run particle minecraft:dust 126 126 126 5 ~ ~ ~ 0.5 0.5 0.5 5 1 normal
scoreboard players reset @s[scores={DMZBowUse=1..}] DMZBowUse
scoreboard players add @s[tag=RainBowArrow,nbt={inGround:0b}] Ticker1 1
scoreboard players reset @s[tag=RainBowArrow,nbt={inGround:0b},scores={Ticker1=3..}] Ticker1