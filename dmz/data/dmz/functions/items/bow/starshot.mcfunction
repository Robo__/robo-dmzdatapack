#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Starshot - Shoots a ghast fireball.  Sort of.  It's wonky.
execute as @s[scores={DMZBowUse=1}] at @s run playsound minecraft:entity.generic.explode master @a[distance=..20] ~ ~ ~ 1.1 1 1
execute as @s[scores={DMZBowUse=1}] at @s run summon minecraft:fireball ^ ^1 ^2 {direction:[0.0,0.0,0.0],power:[0.0,0.0,0.0],Life:20,ExplosionPower:1,Tags:["StarFireball","CItemEntity"]}
effect give @s[type=player] minecraft:slowness 1 5 true
effect give @s[type=player] minecraft:jump_boost 1 200 true
scoreboard players reset @s[scores={DMZBowUse=1..}] DMZBowUse
execute as @e[type=fireball,tag=StarFireball,scores={Ticker1=35..35}] at @s run summon minecraft:bat ^ ^ ^-1 {Silent:1,ActiveEffects:[{Id:14,Amplifier:1,Duration:200,ShowParticles:1b},{Id:20,Amplifier:10,Duration:200,ShowParticles:1b}]}
kill @e[type=fireball,tag=StarFireball,scores={Ticker1=40..}]
scoreboard players add @e[type=fireball,tag=StarFireball] Ticker1 1
