#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Robow - A laser bow. As the arrow flies it catches living creatures in a radius of 3 blocks on fire and outlines the target that was struck.
execute as @s[scores={DMZBowUse=1}] at @s run playsound minecraft:entity.dolphin.death master @a[distance=..10] ~ ~ ~ 1 0.8 .5
execute as @s[scores={DMZBowUse=1}] at @s as @e[type=arrow,distance=..5] run data merge entity @s {Tags:["RobowArrow","CItemEntity"],NoGravity:1b,pickup:0,life:1,CustomPotionEffects:[{Id:24,Amplifier:0,Duration:200,ShowParticles:0b}],Potion:"minecraft:awkward"}
execute as @s[tag=RobowArrow,type=arrow] at @s as @e[type=!arrow,type=!item,type=!item_frame,distance=0..1.75] run data merge entity @s {Fire:80}
execute as @e[tag=RobowArrow,type=arrow] at @s run particle minecraft:dust 255 0 0 1 ~ ~ ~ 0.2 0.2 0.2 0 6
scoreboard players reset @s[scores={DMZBowUse=1..}] DMZBowUse 
scoreboard players add @s[type=arrow,tag=RobowArrow] Ticker1 1

kill @s[type=arrow,tag=RobowArrow,scores={Ticker1=250..}]