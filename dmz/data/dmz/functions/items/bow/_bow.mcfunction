#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |      DMZCraft MCFuntion file that for custom bows            |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Rainbow - Taste the rainbow. Shoots a rainbow of colors restoring health and hunger to those hit.
execute as @s[scores={DMZBowUse=1..},nbt={SelectedItem:{tag:{RainBow:1b}}}] run function dmz:items/bow/rainbow
execute as @s[scores={DMZBowUse=1..},nbt={Inventory:[{Slot:-106b,tag:{RainBow:1b}}]}] run function dmz:items/bow/rainbow
#
# Robow - A laser bow. As the arrow flies it catches living creatures in a radius of 3 blocks on fire and outlines the target that was struck.
execute as @s[scores={DMZBowUse=1..},nbt={SelectedItem:{tag:{Robow:1b}}}] run function dmz:items/bow/robow
execute as @s[scores={DMZBowUse=1..},nbt={Inventory:[{Slot:-106b,tag:{Robow:1b}}]}] run function dmz:items/bow/robow
#
# Starshot - Shoots a ghast fireball.  Sort of.  It's wonky.
execute as @s[scores={DMZBowUse=1..},nbt={SelectedItem:{tag:{Starshot:1b}}}] run function dmz:items/bow/starshot
execute as @s[scores={DMZBowUse=1..},nbt={Inventory:[{Slot:-106b,tag:{Starshot:1b}}]}] run function dmz:items/bow/starshot

