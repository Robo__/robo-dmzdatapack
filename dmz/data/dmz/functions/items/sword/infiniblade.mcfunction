#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Infiniblade - Slows down creatures around you when used
execute as @s[scores={DMZDDSwordUse=1}] at @s run playsound minecraft:entity.elder_guardian.curse master @a[distance=..20] ~ ~ ~ 0.3 0.95 1
execute as @s[scores={DMZDDSwordUse=1}] at @s run effect give @e[type=!player,type=!item,type=!item_frame,distance=0..5] minecraft:slowness 2 2
execute as @s[scores={DMZDDSwordUse=1}] at @s run effect give @e[type=!player,type=!item,type=!item_frame,distance=0..5] minecraft:mining_fatigue 2 2
execute as @s[scores={DMZDDSwordUse=1}] at @s anchored eyes run particle minecraft:falling_dust sand ^ ^ ^2 0.6 0.6 0.6 0.07 20 normal @s
scoreboard players reset @s[scores={DMZDDSwordUse=1..}] DMZDDSwordUse
