#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Mr. Blackmist - Spray of black when used to hit something.
tag @s[scores={DMZDDSwordUse=1},nbt={SelectedItem:{tag:{MrBlackmist:1b}}}] add BlackmistUsed
execute as @s[tag=BlackmistUsed] at @s run particle minecraft:item black_stained_glass ^ ^1.5 ^1.5 0.5 0.5 0.5 0.3 80
execute as @s[tag=BlackmistUsed] at @s run particle minecraft:falling_dust black_stained_glass ^ ^1.5 ^1.5 1 1 1 1 80 normal
execute as @s[tag=BlackmistUsed] at @s run playsound minecraft:entity.zombie.attack_door_wood master @a[distance=..15] ~ ~ ~ 0.5 0.5 1
scoreboard players reset @s[scores={DMZDDSwordUse=1..}] DMZDDSwordUse
tag @s[tag=BlackmistUsed] remove BlackmistUsed
