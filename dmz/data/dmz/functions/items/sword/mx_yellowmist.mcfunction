#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Mr. Yellowmist - Spray of yellow when used to hit something.
tag @s[scores={DMZDDSwordUse=1},nbt={SelectedItem:{tag:{MxYellowmist:1b}}}] add YellowmistUsed
execute as @s[tag=YellowmistUsed] at @s run particle minecraft:item yellow_stained_glass ^ ^1.5 ^1.5 0.5 0.5 0.5 0.3 80
execute as @s[tag=YellowmistUsed] at @s run particle minecraft:falling_dust yellow_stained_glass ^ ^1.5 ^1.5 1 1 1 1 80 normal
execute as @s[tag=YellowmistUsed] at @s run playsound minecraft:entity.zombie.attack_door_wood master @a[distance=..15] ~ ~ ~ 0.5 0.5 1
scoreboard players reset @s[scores={DMZDDSwordUse=1..}] DMZDDSwordUse
tag @s[tag=YellowmistUsed] remove YellowmistUsed
