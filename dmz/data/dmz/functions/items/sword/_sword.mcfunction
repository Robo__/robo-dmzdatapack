#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |      DMZCraft MCFuntion file that for custom swords          |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Infiniblade - Slows creatures around you down.
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{Infiniblade:1b}}}] run function dmz:items/sword/infiniblade
#
# Mr. Xmist - Spray of color when used to hit something
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MsPinkmist:1b}}}] run function dmz:items/sword/ms_pinkmist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MsRedmist:1b}}}] run function dmz:items/sword/ms_redmist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MsOrangemist:1b}}}] run function dmz:items/sword/ms_orangemist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MxYellowmist:1b}}}] run function dmz:items/sword/mx_yellowmist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MxGreenmist:1b}}}] run function dmz:items/sword/mx_greenmist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MrBluemist:1b}}}] run function dmz:items/sword/mr_bluemist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MrIndigomist:1b}}}] run function dmz:items/sword/mr_indigomist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MrVioletmist:1b}}}] run function dmz:items/sword/mr_violetmist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MrBlackmist:1b}}}] run function dmz:items/sword/mr_blackmist
execute as @s[scores={DMZDDSwordUse=1..},nbt={SelectedItem:{tag:{MsWhitemist:1b}}}] run function dmz:items/sword/ms_whitemist
