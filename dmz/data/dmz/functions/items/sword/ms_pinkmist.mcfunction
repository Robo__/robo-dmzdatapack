#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Mr. Pinkmist - Spray of pink when used to hit something.
tag @s[scores={DMZDDSwordUse=1},nbt={SelectedItem:{tag:{MsPinkmist:1b}}}] add PinkmistUsed
execute as @s[tag=PinkmistUsed] at @s run particle minecraft:item pink_stained_glass ^ ^1.5 ^1.5 0.5 0.5 0.5 0.3 80
execute as @s[tag=PinkmistUsed] at @s run particle minecraft:falling_dust pink_stained_glass ^ ^1.5 ^1.5 1 1 1 1 80 normal
execute as @s[tag=PinkmistUsed] at @s run playsound minecraft:entity.zombie.attack_door_wood master @a[distance=..15] ~ ~ ~ 0.5 0.5 1
scoreboard players reset @s[scores={DMZDDSwordUse=1..}] DMZDDSwordUse
tag @s[tag=PinkmistUsed] remove PinkmistUsed