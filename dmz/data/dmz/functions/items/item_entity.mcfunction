#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |     DMZCraft MCFuntion file that is executed for item entity |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Starshot - Shoots a ghast fireball.  Sort of.  It's wonky.
execute as @e[type=fireball,tag=StarFireball] run function dmz:items/bow/starshot
# Cmdblock Power Off Wand - Makes nearby command blocks "need redstone".
execute as @e[type=armor_stand,tag=cmdoffarmor] run function dmz:items/wand/wand_cmdoff
# Robow - A laser bow. As the arrow flies it catches living creatures in a radius of 3 blocks on fire and outlines the target that was struck.
execute as @e[tag=RobowArrow,type=arrow] run function dmz:items/bow/robow
# Rainbow - Taste the rainbow. Shoots a rainbow of colors restoring health and hunger to those hit.
execute as @e[tag=RainBowArrow,type=arrow,nbt={inGround:0b}] run function dmz:items/bow/rainbow
# Damage wand
execute as @s[type=armor_stand,tag=bullet] at @s run function dmz:items/wand/damagewand
# Health wand
execute as @s[type=armor_stand,tag=healbullet] at @s run function dmz:items/wand/healthwand
# Fire wand
execute as @s[type=armor_stand,tag=firebullet] at @s run function dmz:items/wand/firewand
# Ice wand
execute as @s[type=armor_stand,tag=waterbullet] at @s run function dmz:items/wand/icewand
# Electric wand
execute as @s[type=armor_stand,tag=elecbullet] at @s run function dmz:items/wand/elecwand
# Dark Wand
execute as @s[type=armor_stand,tag=voidbullet] at @s run function dmz:items/wand/darkwand
# Fragmentation Wand
execute as @s[type=armor_stand,tag=fragbullet] at @s run function dmz:items/wand/fragwand
# Artillery Wand
execute as @s[type=armor_stand,tag=arrowbullet] at @s run function dmz:items/wand/arrowwand
# Levitation Wand
execute as @s[type=armor_stand,tag=levibullet] at @s run function dmz:items/wand/leviwand
# Kill entities
execute as @s run scoreboard players add @s DummyTracker1 1
execute as @s[scores={DummyTracker1=200..}] run kill @s
# Custom kill counter
execute as @s[scores={DummyTracker1=80..},tag=ShortLife] run kill @s

