#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Glass Shovel - Automatically produces glass from sand.
execute as @s[scores={DMZDDShovelUse=1}] at @s run tag @e[type=item,nbt={Age:0s,Item:{id:"minecraft:sand"}},distance=..10] add SandDrop
execute as @e[type=item,tag=SandDrop] run data merge entity @s {Item:{id:"minecraft:glass"}}
execute as @e[tag=SandDrop] at @s run particle minecraft:lava ~ ~ ~ 0.3 0.3 0.3 0 5 normal @a
execute as @e[tag=SandDrop] at @s run playsound minecraft:entity.generic.burn master @a ~ ~ ~ 1 1 0
tag @e[tag=SandDrop] remove SandDrop
scoreboard players reset @s[scores={DMZDDShovelUse=1..}] DMZDDShovelUse
