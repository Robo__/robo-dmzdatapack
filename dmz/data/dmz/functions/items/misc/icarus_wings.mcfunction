#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wings of Icarus - Elytra that give the player a weak rocket after 220 ticks, and a strong rocket after 1220. Timer resets when rocket is used.
execute as @s[scores={DMZRocketUse=1..}] run clear @s minecraft:firework_rocket{EndlessRocket:1b}
scoreboard players reset @s[scores={DMZRocketUse=1..}] DMZRocketUse
execute as @s[nbt={Inventory:[{tag:{EndlessRocket:1b}}]}] run tag @s add HasEndlessRocket
execute as @s[tag=HasEndlessRocket] run scoreboard players reset @s ElytraCheck
scoreboard players add @s[tag=!HasEndlessRocket,nbt={Inventory:[{tag:{IcarusWings:1b},Slot:102b}]}] ElytraCheck 1
# execute as @a[y=330,distance=..100,nbt={Inventory:[{tag:{IcarusWings:1b},Slot:102b}]}] at @a[y=330,distance=..100,nbt={Inventory:[{tag:{IcarusWings:1b},Slot:102b}]}] run summon minecraft:arrow ~ ~2.1 ~ {pickup:0,damage:0,Fire:1000}
tag @s[tag=HasEndlessRocket] remove HasEndlessRocket
execute as @s[scores={ElytraCheck=195..195}] run give @s minecraft:firework_rocket{display:{Name:"{\"text\":\"Icarian Rocket\",\"color\":\"yellow\"}",Lore:["You don't want to know where this came from..."]},HideFlags:1,EndlessRocket:1b,Enchantments:[{id:"-1",lvl:1}],Fireworks:{Flight:-1}} 1

