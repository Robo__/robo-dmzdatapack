#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Morphtool - A tool that changes type every time you drop it.
scoreboard players set @e[nbt={Item:{id:"minecraft:diamond_sword",tag:{Morphtool:1b}}},type=minecraft:item,tag=!Morph] Morphtool 1
scoreboard players set @e[nbt={Item:{id:"minecraft:diamond_pickaxe",tag:{Morphtool:1b}}},type=minecraft:item,tag=!Morph] Morphtool 2
scoreboard players set @e[nbt={Item:{id:"minecraft:diamond_axe",tag:{Morphtool:1b}}},type=minecraft:item,tag=!Morph] Morphtool 3
scoreboard players set @e[nbt={Item:{id:"minecraft:diamond_shovel",tag:{Morphtool:1b}}},type=minecraft:item,tag=!Morph] Morphtool 4
scoreboard players set @e[nbt={Item:{id:"minecraft:diamond_hoe",tag:{Morphtool:1b}}},type=minecraft:item,tag=!Morph] Morphtool 5
execute as @e[type=minecraft:item,tag=!Morph,scores={Morphtool=1..1}] run data merge entity @s {Tags:["Morph"],Item:{id:"minecraft:diamond_pickaxe"},PickupDelay:0}
execute as @e[type=minecraft:item,tag=!Morph,scores={Morphtool=2..2}] run data merge entity @s {Tags:["Morph"],Item:{id:"minecraft:diamond_axe"},PickupDelay:0}
execute as @e[type=minecraft:item,tag=!Morph,scores={Morphtool=3..3}] run data merge entity @s {Tags:["Morph"],Item:{id:"minecraft:diamond_shovel"},PickupDelay:0}
execute as @e[type=minecraft:item,tag=!Morph,scores={Morphtool=4..4}] run data merge entity @s {Tags:["Morph"],Item:{id:"minecraft:diamond_hoe"},PickupDelay:0}
execute as @e[type=minecraft:item,tag=!Morph,scores={Morphtool=5..5}] run data merge entity @s {Tags:["Morph"],Item:{id:"minecraft:diamond_sword"},PickupDelay:0}
