#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |      DMZCraft MCFuntion file that for custom misc. items     |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
#
#Vampire Curio A small heal when you kill a target while holding one of these in your off-hand.
execute as @s[scores={CurioKill=1..},nbt={Inventory:[{tag:{VampireCurio:1b},Slot:-106b}]}] run function dmz:items/misc/curio_vampire
#
# Scholar's Curio - Grants 2 extra XP when held in off-hand while picking up xp
execute as @s[nbt={Inventory:[{tag:{ScholarsCurio:1b},Slot:-106b}]}] run function dmz:items/misc/curio_scholars
#
# Wings of Icarus - Elytra that give the player a weak rocket after 210 ticks. Timer resets when rocket is used.
# Eats all extras of that rocket type when one is used so players cannot hoard them.
execute as @s[nbt={Inventory:[{tag:{IcarusWings:1b},Slot:102b}]}] run function dmz:items/misc/icarus_wings
