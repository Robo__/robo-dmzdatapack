#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |     DMZCraft MCFuntion file that is executed when item found |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
##### Custom item checks
# Swords
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomSword:1b}}]}] run function dmz:items/sword/_sword
execute as @s[nbt={SelectedItem:{tag:{CustomSword:1b}}}] run function dmz:items/sword/_sword
# Axes
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomAxe:1b}}]}] run function dmz:items/axe/_axe
execute as @s[nbt={SelectedItem:{tag:{CustomAxe:1b}}}] run function dmz:items/axe/_axe
# Pickaxes
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomPickaxe:1b}}]}] run function dmz:items/pickaxe/_pickaxe
execute as @s[nbt={SelectedItem:{tag:{CustomPickaxe:1b}}}] run function dmz:items/pickaxe/_pickaxe
# Shovels
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomShovel:1b}}]}] run function dmz:items/shovel/_shovel
execute as @s[nbt={SelectedItem:{tag:{CustomShovel:1b}}}] run function dmz:items/shovel/_shovel
# Hoes
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomHoe:1b}}]}] run function dmz:items/hoe/_hoe
execute as @s[nbt={SelectedItem:{tag:{CustomHoe:1b}}}] run function dmz:items/hoe/_hoe
# Bows
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomBow:1b}}]}] run function dmz:items/bow/_bow
execute as @s[nbt={SelectedItem:{tag:{CustomBow:1b}}}] run function dmz:items/bow/_bow
# Wands
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] run function dmz:items/wand/_wand
execute as @s[nbt={SelectedItem:{tag:{CustomWand:1b}}}] run function dmz:items/wand/_wand
# Tridents
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomTrident:1b}}]}] run function dmz:items/trident/_trident
execute as @s[nbt={SelectedItem:{tag:{CustomTrident:1b}}}] run function dmz:items/trident/_trident
# Misc
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{CustomMisc:1b}}]}] run function dmz:items/misc/_misc
execute as @s[nbt={SelectedItem:{tag:{CustomMisc:1b}}}] run function dmz:items/misc/_misc
#
#
#volley
execute as @e[type=area_effect_cloud,tag=Volley] at @s run function dmz:items/wand/arrowwand





