#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Darkness - Creates a blinding cloud and puts wither on targets
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..4}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..4}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["voidbullet", "CItemEntity"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=voidbullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=voidbullet,distance=0..2] ^ ^ ^3

#sound
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run playsound block.end_portal.spawn master @a[distance=0..20] ~ ~ ~ 0.7 1 0.7

#take mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=5..}] Mana 5

#dont take Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=5..}] Mana 5

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle
execute as @s[type=armor_stand,tag=voidbullet] at @s run particle portal ~ ~ ~ 0.5 0.5 0.5 0.1 4
execute as @s[type=armor_stand,tag=voidbullet] at @s run particle squid_ink ~ ~ ~ 0.2 0.2 0.2 0.1 1

#movement
execute as @s[type=armor_stand,tag=voidbullet] at @s run tp @s ^ ^ ^3
execute as @s[type=armor_stand,tag=voidbullet] at @s run teleport @s ~ ~ ~ ~ ~0.1

#effects
execute as @s[type=armor_stand,tag=voidbullet] at @s run effect give @e[distance=0..4] wither 5 3 false

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=voidbullet] at @s unless block ~ ~ ~ air run summon area_effect_cloud ~ ~1.2 ~ {Particle:"portal",ReapplicationDelay:1,Radius:10f,RadiusPerTick:-0.06f,RadiusOnUse:0.04f,Duration:1000,DurationOnUse:-0.02f,Effects:[{Id:20b,Amplifier:3b,Duration:60,ShowParticles:1b}]}
execute as @s[type=armor_stand,tag=voidbullet] at @s unless block ~ ~ ~ air run particle portal ^ ^ ^-1 1 4 1 0.1 40
execute as @s[type=armor_stand,tag=voidbullet] at @s unless block ~ ~ ~ air run particle squid_ink ^ ^ ^-1 0 2 0 0.1 40
execute as @s[type=armor_stand,tag=voidbullet] at @s unless block ~ ~ ~ air run kill @s
