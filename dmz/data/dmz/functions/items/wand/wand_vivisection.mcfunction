#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Vivisection - Harms nearby animals to produce meat. Costs XP to use.
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..4}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..4}] DMZWandUse
#
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run playsound minecraft:entity.player.big_fall master @s ~ ~ ~ 1 1 0
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run playsound minecraft:entity.player.attack.crit master @s ~ ~ ~ 1 1 0
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run playsound minecraft:entity.player.attack.sweep master @s ~ ~ ~ 1 2 0
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:chicken,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:chicken,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:chicken,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:chicken,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:cow,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:cow,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:beef,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:cow,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:mooshroom,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:mooshroom,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:beef,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:mooshroom,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:pig,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:pig,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:porkchop,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:pig,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:rabbit,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:rabbit,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:rabbit,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:rabbit,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:sheep,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:sheep,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:mutton,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:sheep,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:cod,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:cod,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:cod,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:cod,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:salmon,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:salmon,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:salmon,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:salmon,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:pufferfish,distance=0..5] run particle minecraft:sweep_attack ~ ~ ~ 0.3 0.3 0.3 0 3 normal
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:pufferfish,distance=0..5] run summon minecraft:item ~ ~ ~ {Item:{id:pufferfish,Count:1b}}
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s as @e[type=minecraft:pufferfish,distance=0..5] run effect give @s minecraft:instant_damage 1 0 true
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=5..}] Mana 5
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse
