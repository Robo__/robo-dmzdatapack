#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Reproduction - Causes farm animals and woodland creatures to become rather becoming to one another when used around them. Costs XP to cast.
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..19}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..19}] DMZWandUse
#
execute as @s[scores={DMZWandUse=1..,Mana=20..}] at @s run playsound minecraft:block.brewing_stand.brew master @s ~ ~ ~ 1 0.7 0
execute at @s[scores={DMZWandUse=1..,Mana=20..}] at @s as @e[type=!item_frame,type=!item,type=!player,distance=..10] run data merge entity @s {InLove:1200}
execute at @s[scores={DMZWandUse=1..,Mana=20..}] at @s run particle minecraft:heart ~ ~2 ~ 2 1 2 0 20 normal
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=20..}] Mana 20
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse
