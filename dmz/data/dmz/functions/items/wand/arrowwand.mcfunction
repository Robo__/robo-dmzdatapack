#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of arrowmentaion - Destroys harmful projectiles in front of the user
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..0}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..0}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1,Mana=4..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["arrowbullet", "CItemEntity","ShortLife"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=arrowbullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=arrowbullet,distance=0..2] ^ ^ ^2

#sound
execute as @s[scores={DMZWandUse=1..,Mana=4..},nbt={SelectedItem:{tag:{ArrowWand:1b}}}] at @s run playsound minecraft:entity.shulker.shoot master @a[distance=0..20] ~ ~ ~ 0.8 1.4 0.8

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=4..},nbt={SelectedItem:{tag:{arrowbulletWand:1b}}}] Mana 3

#don't replenish Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=4..}] Mana 4

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle
execute as @s[type=armor_stand,tag=arrowbullet] at @s run particle crit ~ ~ ~ 0.1 0.1 0.1 0.01 4

#movement
execute as @s[type=armor_stand,tag=arrowbullet] at @s run tp @s ^ ^ ^0.6
execute as @s[type=armor_stand,tag=arrowbullet] at @s run teleport @s ~ ~ ~ ~ ~1

#effects



#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=arrowbullet] at @s unless block ~ ~ ~ air run summon area_effect_cloud ~ ~6 ~ {Particle:"take",ReapplicationDelay:999,Radius:0f,RadiusPerTick:0f,RadiusOnUse:0f,Duration:50,DurationOnUse:0f,Tags:["Volley"]}
execute as @s[type=armor_stand,tag=arrowbullet] at @s unless block ~ ~ ~ air run kill @s





#volley
execute as @e[type=area_effect_cloud,tag=Volley] at @s run summon arrow ~ ~ ~ {Motion:[0.0,-1.0,0.0],Tags:["VArrow","CItemEntity"],damage:5d}
execute as @e[type=arrow,tag=VArrow] at @s run spreadplayers ~ ~ 3 3 false @s
execute as @e[type=arrow,tag=VArrow] at @s run teleport @s ~ ~20 ~
tag @e[type=arrow,tag=VArrow] remove VArrow

