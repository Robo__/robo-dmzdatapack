#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Fire - shoots a fireball that catches nearby targets on fire
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..0}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..0}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1..,Mana=1..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["healbullet", "CItemEntity"]}
effect give @s[scores={DMZWandUse=1..,Mana=1..}] instant_health 1 0 true

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=healbullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=healbullet,distance=0..2] ^ ^ ^2

#sound
execute as @s[scores={DMZWandUse=1..,Mana=1..}] at @s run playsound block.brewing_stand.brew master @a[distance=0..20] ~ ~ ~ 0.7 1.6 0.7

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=1..}] Mana 1

#dont take Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=1..}] Mana 1

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity Command

#particle
execute as @s[type=armor_stand,tag=healbullet] at @s run particle minecraft:happy_villager ~ ~ ~ 0.1 0.1 0.1 0.0 2

#effects
execute as @s[type=armor_stand,tag=healbullet] at @s run effect give @e[distance=0..2] instant_health 1 0 true

#movement
execute as @s[type=armor_stand,tag=healbullet] at @s run tp @s ^ ^ ^2
execute as @s[type=armor_stand,tag=healbullet] at @s run teleport @s ~ ~ ~ ~ ~0.1

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=healbullet] at @s unless block ~ ~ ~ air run particle minecraft:happy_villager ^ ^ ^-1 0.5 0.5 0.5 0.06 12
execute as @s[type=armor_stand,tag=healbullet] at @s unless block ~ ~ ~ air run kill @s
execute as @s[type=armor_stand,tag=healbullet] at @s run scoreboard players add @s Ticker1 1
execute as @s[type=armor_stand,tag=healbullet] at @s run kill @s[scores={Ticker1=120..}]
