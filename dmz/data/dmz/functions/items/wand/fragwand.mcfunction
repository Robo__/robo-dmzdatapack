#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Fragmentaion - Destroys harmful projectiles in front of the user
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..0}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..0}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1,Mana=4..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["fragbullet", "CItemEntity","ShortLife"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=fragbullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=fragbullet,distance=0..2] ^ ^ ^2

#sound
execute as @s[scores={DMZWandUse=1..,Mana=4..},nbt={SelectedItem:{tag:{FragWand:1b}}}] at @s run playsound minecraft:entity.armor_stand.break master @a[distance=0..20] ~ ~ ~ 1 0.8 1

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=4..},nbt={SelectedItem:{tag:{fragbulletWand:1b}}}] Mana 3

#don't replenish Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=4..}] Mana 4

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle
execute as @s[type=armor_stand,tag=fragbullet] at @s run particle crit ~ ~ ~ 1 1 1 0.01 4

#movement
execute as @s[type=armor_stand,tag=fragbullet] at @s run tp @s ^ ^ ^0.125
execute as @s[type=armor_stand,tag=fragbullet] at @s run teleport @s ~ ~ ~ ~ ~0.2

#effects

execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=arrow,nbt={inGround:0b},tag=!CItemEntity] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:arrow",Count:1b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=tnt] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:tnt",Count:1b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=fireball] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:fire_charge",Count:2b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=small_fireball] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:fire_charge",Count:1b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=dragon_fireball] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:dragon_breath",Count:1b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=firework_rocket] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:gunpowder",Count:3b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=wither_skull] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:coal",Count:2b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=potion] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:glass_bottle",Count:1b}}
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=spectral_arrow,nbt={inGround:0b}] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:spectral_arrow",Count:1b}}

execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=arrow,nbt={inGround:0b}]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=tnt]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=potion]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=fireball]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=dragon_fireball]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=small_fireball]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=area_effect_cloud]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=firework_rocket]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=wither_skull]
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=shulker_bullet]
execute as @s[type=armor_stand,tag=fragbullet] at @s run execute as @e[distance=0..3.5,type=trident] at @s run data merge entity @s {Motion:[0.0,-1.0,0.0]}
execute as @s[type=armor_stand,tag=fragbullet] at @s run kill @e[distance=0..3.5,type=spectral_arrow]

#kill when in block and spawn particles
#execute as @s[type=armor_stand,tag=fragbullet] at @s unless block ~ ~ ~ air run particle poof ^ ^ ^-1 0.4 0.4 0.4 0.06 4
execute as @s[type=armor_stand,tag=fragbullet] at @s unless block ~ ~ ~ air run kill @s


