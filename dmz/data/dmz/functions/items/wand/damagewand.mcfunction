#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Damage - Deals damage to living targets
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..0}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..0}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1,Mana=1..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["bullet", "CItemEntity"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=bullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=bullet,distance=0..2] ^ ^ ^2

#sound
execute as @s[scores={DMZWandUse=1..,Mana=1..},nbt={SelectedItem:{tag:{BulletWand:1b}}}] at @s run playsound entity.ender_eye.death master @a[distance=0..20] ~ ~ ~ 0.7 1.6 0.7

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=1..},nbt={SelectedItem:{tag:{BulletWand:1b}}}] Mana 1

#don't replenish Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=1..}] Mana 1

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle
execute as @s[type=armor_stand,tag=bullet] at @s run particle instant_effect ~ ~ ~ 0.1 0.1 0.1 0.0 2

#movement
execute as @s[type=armor_stand,tag=bullet] at @s run tp @s ^ ^ ^2
execute as @s[type=armor_stand,tag=bullet] at @s run teleport @s ~ ~ ~ ~ ~0.1

#effects
execute as @s[type=armor_stand,tag=bullet] at @s run effect give @e[distance=0..2] instant_damage 1 0 true

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=bullet] at @s unless block ~ ~ ~ air run particle poof ^ ^ ^-1 0.4 0.4 0.4 0.06 4
execute as @s[type=armor_stand,tag=bullet] at @s unless block ~ ~ ~ air run kill @s


