#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Cavitation - Creates pocket of air underwater. Does not replenish breath.
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..2}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..2}] DMZWandUse
#
execute as @s[scores={DMZWandUse=1..,Mana=3..}] at @s run summon area_effect_cloud ~ ~0.1 ~ {Particle:"bubble",ReapplicationDelay:20,Radius:5f,RadiusPerTick:0f,RadiusOnUse:0f,Duration:500,DurationOnUse:3f,WaitTime:0,Effects:[{Id:13b,Amplifier:1b,Duration:400,ShowParticles:1b}]}
execute as @s[scores={DMZWandUse=1..,Mana=3..}] at @s run playsound minecraft:entity.generic.swim master @s ~ ~ ~ 1 0.4 0
execute as @s[scores={DMZWandUse=1..,Mana=3..}] at @s run particle minecraft:totem_of_undying ~ ~ ~ 0.3 0.3 0.3 .6 10 normal
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=3..}] Mana 3
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

