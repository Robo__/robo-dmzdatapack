#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Cavitation - Creates pocket of air underwater. Does not replenish breath.
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-15 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-14 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-13 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-12 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-11 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-10 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-9 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-8 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-7 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}
execute as @s[scores={DMZWandUse=1}] at @s run summon armor_stand ~-6 ~ ~-15 {Small:1b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["cmdoffarmor","CItemEntity"]}

execute as @s[scores={DMZWandUse=1}] at @s run playsound minecraft:item.trident.return master @s ~ ~ ~ 1.5 0.3 1

#execute as @a[scores={DMZWandUse=1..1},tag=WandCmdOff] at @s run tellraw @a[distance=..17] {"text":"You feel the slight tingle of static.","color":"dark_aqua"}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s run particle item fire_coral ~ ~ ~ 5 15 5 0 2 normal

execute as @s[scores={DMZWandUse=1..}] run scoreboard players reset @s DMZWandUse

scoreboard players add @s[type=armor_stand,tag=cmdoffarmor] Ticker1 1

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~ ~ minecraft:command_block run data merge block ~ ~ ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~ ~ minecraft:chain_command_block run data merge block ~ ~ ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~ ~ minecraft:repeating_command_block run data merge block ~ ~ ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~1 ~ minecraft:command_block run data merge block ~ ~1 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~1 ~ minecraft:chain_command_block run data merge block ~ ~1 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~1 ~ minecraft:repeating_command_block run data merge block ~ ~1 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~2 ~ minecraft:command_block run data merge block ~ ~2 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~2 ~ minecraft:chain_command_block run data merge block ~ ~2 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~2 ~ minecraft:repeating_command_block run data merge block ~ ~2 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~3 ~ minecraft:command_block run data merge block ~ ~3 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~3 ~ minecraft:chain_command_block run data merge block ~ ~3 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~3 ~ minecraft:repeating_command_block run data merge block ~ ~3 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~4 ~ minecraft:command_block run data merge block ~ ~4 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~4 ~ minecraft:chain_command_block run data merge block ~ ~4 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~4 ~ minecraft:repeating_command_block run data merge block ~ ~4 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~5 ~ minecraft:command_block run data merge block ~ ~5 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~5 ~ minecraft:chain_command_block run data merge block ~ ~5 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~5 ~ minecraft:repeating_command_block run data merge block ~ ~5 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~6 ~ minecraft:command_block run data merge block ~ ~6 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~6 ~ minecraft:chain_command_block run data merge block ~ ~6 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~6 ~ minecraft:repeating_command_block run data merge block ~ ~6 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~7 ~ minecraft:command_block run data merge block ~ ~7 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~7 ~ minecraft:chain_command_block run data merge block ~ ~7 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~7 ~ minecraft:repeating_command_block run data merge block ~ ~7 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~8 ~ minecraft:command_block run data merge block ~ ~8 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~8 ~ minecraft:chain_command_block run data merge block ~ ~8 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~8 ~ minecraft:repeating_command_block run data merge block ~ ~8 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~9 ~ minecraft:command_block run data merge block ~ ~9 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~9 ~ minecraft:chain_command_block run data merge block ~ ~9 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~9 ~ minecraft:repeating_command_block run data merge block ~ ~9 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~10 ~ minecraft:command_block run data merge block ~ ~10 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~10 ~ minecraft:chain_command_block run data merge block ~ ~10 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~10 ~ minecraft:repeating_command_block run data merge block ~ ~10 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~11 ~ minecraft:command_block run data merge block ~ ~11 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~11 ~ minecraft:chain_command_block run data merge block ~ ~11 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~11 ~ minecraft:repeating_command_block run data merge block ~ ~11 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~12 ~ minecraft:command_block run data merge block ~ ~12 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~12 ~ minecraft:chain_command_block run data merge block ~ ~12 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~12 ~ minecraft:repeating_command_block run data merge block ~ ~12 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~13 ~ minecraft:command_block run data merge block ~ ~13 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~13 ~ minecraft:chain_command_block run data merge block ~ ~13 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~13 ~ minecraft:repeating_command_block run data merge block ~ ~13 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~14 ~ minecraft:command_block run data merge block ~ ~14 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~14 ~ minecraft:chain_command_block run data merge block ~ ~14 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~14 ~ minecraft:repeating_command_block run data merge block ~ ~14 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~15 ~ minecraft:command_block run data merge block ~ ~15 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~15 ~ minecraft:chain_command_block run data merge block ~ ~15 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~15 ~ minecraft:repeating_command_block run data merge block ~ ~15 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-1 ~ minecraft:command_block run data merge block ~ ~-1 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-1 ~ minecraft:chain_command_block run data merge block ~ ~-1 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-1 ~ minecraft:repeating_command_block run data merge block ~ ~-1 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-2 ~ minecraft:command_block run data merge block ~ ~-2 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-2 ~ minecraft:chain_command_block run data merge block ~ ~-2 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-2 ~ minecraft:repeating_command_block run data merge block ~ ~-2 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-3 ~ minecraft:command_block run data merge block ~ ~-3 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-3 ~ minecraft:chain_command_block run data merge block ~ ~-3 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-3 ~ minecraft:repeating_command_block run data merge block ~ ~-3 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-4 ~ minecraft:command_block run data merge block ~ ~-4 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-4 ~ minecraft:chain_command_block run data merge block ~ ~-4 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-4 ~ minecraft:repeating_command_block run data merge block ~ ~-4 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-5 ~ minecraft:command_block run data merge block ~ ~-5 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-5 ~ minecraft:chain_command_block run data merge block ~ ~-5 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-5 ~ minecraft:repeating_command_block run data merge block ~ ~-5 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-6 ~ minecraft:command_block run data merge block ~ ~-6 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-6 ~ minecraft:chain_command_block run data merge block ~ ~-6 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-6 ~ minecraft:repeating_command_block run data merge block ~ ~-6 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-7 ~ minecraft:command_block run data merge block ~ ~-7 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-7 ~ minecraft:chain_command_block run data merge block ~ ~-7 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-7 ~ minecraft:repeating_command_block run data merge block ~ ~-7 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-8 ~ minecraft:command_block run data merge block ~ ~-8 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-8 ~ minecraft:chain_command_block run data merge block ~ ~-8 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-8 ~ minecraft:repeating_command_block run data merge block ~ ~-8 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-9 ~ minecraft:command_block run data merge block ~ ~-9 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-9 ~ minecraft:chain_command_block run data merge block ~ ~-9 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-9 ~ minecraft:repeating_command_block run data merge block ~ ~-9 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-10 ~ minecraft:command_block run data merge block ~ ~-10 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-10 ~ minecraft:chain_command_block run data merge block ~ ~-10 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-10 ~ minecraft:repeating_command_block run data merge block ~ ~-10 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-11 ~ minecraft:command_block run data merge block ~ ~-11 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-11 ~ minecraft:chain_command_block run data merge block ~ ~-11 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-11 ~ minecraft:repeating_command_block run data merge block ~ ~-11 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-12 ~ minecraft:command_block run data merge block ~ ~-12 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-12 ~ minecraft:chain_command_block run data merge block ~ ~-12 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-12 ~ minecraft:repeating_command_block run data merge block ~ ~-12 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-13 ~ minecraft:command_block run data merge block ~ ~-13 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-13 ~ minecraft:chain_command_block run data merge block ~ ~-13 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-13 ~ minecraft:repeating_command_block run data merge block ~ ~-13 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-14 ~ minecraft:command_block run data merge block ~ ~-14 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-14 ~ minecraft:chain_command_block run data merge block ~ ~-14 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-14 ~ minecraft:repeating_command_block run data merge block ~ ~-14 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-15 ~ minecraft:command_block run data merge block ~ ~-15 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-15 ~ minecraft:chain_command_block run data merge block ~ ~-15 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s if block ~ ~-15 ~ minecraft:repeating_command_block run data merge block ~ ~-15 ~ {auto:0b}

execute as @s[type=armor_stand,tag=cmdoffarmor] at @s run tp @s ~10 ~ ~

execute as @s[type=armor_stand,tag=cmdoffarmor,scores={Ticker1=3}] at @s run tp @s ~-30 ~ ~1

scoreboard players add @s[type=armor_stand,tag=cmdoffarmor,scores={Ticker1=3}] Ticker2 1

scoreboard players reset @s[type=armor_stand,tag=cmdoffarmor,scores={Ticker1=3..}] Ticker1

execute as @s[type=armor_stand,tag=cmdoffarmor,scores={Ticker2=30}] at @s run kill @s
