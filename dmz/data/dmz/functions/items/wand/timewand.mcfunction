#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Time - changes time - mod only
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..19}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..19}] DMZWandUse



#MAINHAND FUNCTION

#sound
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={SelectedItem:{tag:{TimeWand:1b}}}] at @s run playsound minecraft:block.beacon.activate master @a ~ ~ ~ 1 0.6 1
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={SelectedItem:{tag:{TimeWand:1b}}}] at @s run playsound entity.player.levelup master @a ~ ~ ~ 0.6 0.5 0.6
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={SelectedItem:{tag:{TimeWand:1b}}}] at @s run particle end_rod ~ ~5 ~ 2 5 2 0.1 80
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={SelectedItem:{tag:{TimeWand:1b}}}] at @a run particle end_rod ~ ~5 ~ 20 5 20 0.1 40

execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={SelectedItem:{tag:{TimeWand:1b}}}] at @s run time add 8000

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=20..},nbt={SelectedItem:{tag:{TimeWand:1b}}}] Mana 20

#OFFHAND FUNCTION

#sound
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]}] at @s run playsound minecraft:block.beacon.activate master @a ~ ~ ~ 1 0.6 1
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]}] at @s run playsound entity.player.levelup master @a ~ ~ ~ 0.6 0.5 0.6
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]}] at @s run particle end_rod ~ ~5 ~ 2 5 2 0.1 80
execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]}] at @a run particle end_rod ~ ~5 ~ 20 5 20 0.1 40

execute as @s[scores={DMZWandUse=1..,Mana=20..},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]}] at @s run time add 16000



#dont take Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..},nbt={SelectedItem:{tag:{Magic:1b}}}] Repl 0
scoreboard players set @s[scores={DMZWandUse=1..},nbt={Inventory:[{Slot:-106b,tag:{Magic:1b}}]}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=20..},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]}] Mana 20

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse
