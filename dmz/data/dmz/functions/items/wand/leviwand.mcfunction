#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Damage - Deals damage to living targets
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..0}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=2..,Mana=..0}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1,Mana=2..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["levibullet", "CItemEntity"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=levibullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=levibullet,distance=0..2] ^ ^ ^3.5

#sound
execute as @s[scores={DMZWandUse=1..,Mana=2..},nbt={SelectedItem:{tag:{LeviWand:1b}}}] at @s run playsound entity.ender_eye.death master @a[distance=0..20] ~ ~ ~ 0.7 1.6 0.7

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=2..},nbt={SelectedItem:{tag:{levibulletWand:1b}}}] Mana 2

#don't replenish Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=2..}] Mana 2

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle

execute as @s[type=armor_stand,tag=levibullet] at @s run particle end_rod ~ ~ ~ 0.1 0.1 0.1 0.015 1

#movement
execute as @s[type=armor_stand,tag=levibullet] at @s run tp @s ^ ^ ^0.25
execute as @s[type=armor_stand,tag=levibullet] at @s run teleport @s ~ ~ ~ ~ ~-1

#effects
execute as @s[type=armor_stand,tag=levibullet] at @s run effect give @e[distance=0..2] levitation 1 2 true

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=levibullet] at @s unless block ~ ~ ~ air run particle item minecraft:shulker_shell ^ ^ ^-1 0.5 0.5 0.5 0.06 10
execute as @s[type=armor_stand,tag=levibullet] at @s unless block ~ ~ ~ air run kill @s


