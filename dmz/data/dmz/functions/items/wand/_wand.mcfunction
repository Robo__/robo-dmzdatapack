#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |      DMZCraft MCFuntion file that for custom wands           |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Wand of Reproduction - Causes farm animals and woodland creatures to become rather becoming to one another when used around them. Costs XP to cast.
execute as @s[scores={DMZWandUse=1..},nbt={SelectedItem:{tag:{WandReproduction:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] run function dmz:items/wand/wand_reproduction
execute as @s[scores={DMZWandUse=1..},nbt={Inventory:[{Slot:-106b,tag:{WandReproduction:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] run function dmz:items/wand/wand_reproduction
#
# Wand of Cavitation - Creates a pocket of bubbles to breathe in underwater. Does not replenish breath.
execute as @s[scores={DMZWandUse=1..},nbt={SelectedItem:{tag:{WandCavitation:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] run function dmz:items/wand/wand_cavitation
execute as @s[scores={DMZWandUse=1..},nbt={Inventory:[{Slot:-106b,tag:{WandCavitation:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] run function dmz:items/wand/wand_cavitation
#
# Wand of Restoration - Restores HP to those in area of effect. Costs XP to use.
execute as @s[scores={DMZWandUse=1..},nbt={SelectedItem:{tag:{WandRestoration:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] run function dmz:items/wand/wand_restoration
execute as @s[scores={DMZWandUse=1..},nbt={Inventory:[{Slot:-106b,tag:{WandRestoration:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] run function dmz:items/wand/wand_restoration
#
# Wand of Vivisection - Harms nearby animals to produce meat. Costs XP to use.
execute as @s[scores={DMZWandUse=1..},nbt={SelectedItem:{tag:{WandVivisection:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] run function dmz:items/wand/wand_vivisection
execute as @s[scores={DMZWandUse=1..},nbt={Inventory:[{Slot:-106b,tag:{WandVivisection:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] run function dmz:items/wand/wand_vivisection
#
# Cmdblock Power Off Wand - Makes nearby command blocks "need redstone".
execute as @s[scores={DMZWandUse=1..},nbt={SelectedItem:{tag:{WandCmdOff:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] run function dmz:items/wand/wand_cmdoff
#
#damagewand/////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{BulletWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/damagewand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{BulletWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/damagewand
#
#healthwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{HealthWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/healthwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{HealthWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/healthwand
#
#firewand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{FireWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/firewand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{FireWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/firewand
#
#waterwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{WaterWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/icewand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{WaterWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/icewand
#
#elecwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{ElecWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/elecwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{ElecWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/elecwand
#
#darkwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{VoidWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/darkwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{VoidWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/darkwand
#
#timewand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{TimeWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/timewand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{TimeWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/timewand
#
#weatherwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{WeatherWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/weatherwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{WeatherWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/weatherwand
#
#fragmentationwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{FragWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/fragwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{FragWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/fragwand
#
#artillerywand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{ArrowWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/arrowwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{ArrowWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/arrowwand
#
#levitationwand////////////
execute as @s[scores={DMZWandUse=1},nbt={SelectedItem:{tag:{LeviWand:1b}}},nbt=!{Inventory:[{Slot:-106b,tag:{CustomWand:1b}}]}] at @s run function dmz:items/wand/leviwand
execute as @s[scores={DMZWandUse=1},nbt={Inventory:[{Slot:-106b,tag:{LeviWand:1b}}]},nbt=!{SelectedItem:{tag:{CustomWand:1b}}}] at @s run function dmz:items/wand/leviwand
