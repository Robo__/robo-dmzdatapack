#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Frost - fires a ball of ice that slows targets and freezes water
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..1}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..1}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1..,Mana=2..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["waterbullet", "CItemEntity"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=waterbullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=waterbullet,distance=0..2] ^ ^ ^5.5

#sound
execute as @s[scores={DMZWandUse=1..,Mana=2..}] at @s run playsound minecraft:entity.zombie_villager.cure master @a[distance=0..20] ~ ~ ~ 0.5 1.3 0.5

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=2..}] Mana 2

#dont take Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=2..}] Mana 2

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle
execute as @s[type=armor_stand,tag=waterbullet] at @s run particle block minecraft:packed_ice ~ ~ ~ 0.8 0.8 0.8 0.01 3
execute as @s[type=armor_stand,tag=waterbullet] at @s run particle block minecraft:snow ~ ~ ~ 0.2 0.2 0.2 0.0 5

#movement
execute as @s[type=armor_stand,tag=waterbullet] at @s run tp @s ^ ^ ^1
execute as @s[type=armor_stand,tag=waterbullet] at @s run teleport @s ~ ~ ~ ~ ~0.2

#effects
execute as @s[type=armor_stand,tag=waterbullet] at @s at @e[type=armor_stand,tag=waterbullet] run fill ~1 ~1 ~1 ~-1 ~-2 ~-1 minecraft:frosted_ice[age=0] replace minecraft:water
execute as @s[type=armor_stand,tag=waterbullet] at @s at @e[tag=!waterbullet,distance=0..5,type=!item,type=!experience_orb,type=!item_frame,type=!armor_stand,type=!arrow,type=!egg,type=!snowball,type=!potion,type=!leash_knot,type=!painting,type=!llama_spit,type=!area_effect_cloud,type=!trident,type=!painting,type=!llama_spit,type=!area_effect_cloud] run particle block minecraft:packed_ice ~ ~0.5 ~ 0.4 1 0.4 0.01 5
execute as @s[type=armor_stand,tag=waterbullet] at @s run effect give @e[distance=0..5] slowness 5 3 true
execute as @s[type=armor_stand,tag=waterbullet] at @s run effect give @e[distance=0..5] mining_fatigue 5 2 true

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=waterbullet] at @s unless block ~ ~ ~ air run particle block minecraft:packed_ice ^ ^ ^-1 1 1 1 0.1 20
execute as @s[type=armor_stand,tag=waterbullet] at @s unless block ~ ~ ~ air run kill @s
