#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Restoration - Restores HP to those in area of effect. Costs XP to use.
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..7}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..7}] DMZWandUse
#
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s run summon area_effect_cloud ~ ~0.1 ~ {Color:8585115,ReapplicationDelay:30,Radius:4f,RadiusPerTick:0f,RadiusOnUse:0f,Duration:200,DurationOnUse:0f,WaitTime:0,Effects:[{Id:10,Amplifier:2,Duration:50}]}
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s run playsound minecraft:item.trident.riptide_3 master @s ~ ~ ~ 1 0.4 0
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s run particle minecraft:totem_of_undying ~ ~ ~ 0.3 0.3 0.3 .6 10 normal
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:zombie] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:skeleton] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:drowned] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:stray] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:phantom] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:husk] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:zombie_pigman] run effect give @s minecraft:wither 14 5 false
execute as @s[scores={DMZWandUse=1..,Mana=8..}] at @s as @e[distance=..4,type=minecraft:vex] run effect give @s minecraft:wither 14 5 false
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=8..}] Mana 8
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse
