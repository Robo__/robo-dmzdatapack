#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Lightning - smites the target with lightning
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..4}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..4}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["elecbullet", "CItemEntity"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=elecbullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=elecbullet,distance=0..2] ^ ^ ^3

#sound
execute as @s[scores={DMZWandUse=1..,Mana=5..}] at @s run playsound block.end_portal.spawn master @a[distance=0..20] ~ ~ ~ 0.7 1.5 0.7

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=5..}] Mana 5

#dont take Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=5..}] Mana 5

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity commands

#particle
execute as @s[type=armor_stand,tag=elecbullet] at @s run particle end_rod ~ ~ ~ 0.3 0.3 0.3 0.01 5

#movement
execute as @s[type=armor_stand,tag=elecbullet] at @s run tp @s ^ ^ ^3
execute as @s[type=armor_stand,tag=elecbullet] at @s run teleport @s ~ ~ ~ ~ ~0.1

#effects
execute as @s[type=armor_stand,tag=elecbullet] at @s at @e[tag=!elecbullet,distance=0..2,type=!lightning_bolt,type=!item,type=!experience_orb,type=!item_frame,type=!armor_stand,type=!arrow,type=!egg,type=!snowball,type=!potion,type=!leash_knot,type=!painting,type=!llama_spit,type=!area_effect_cloud,type=!trident,type=!painting,type=!llama_spit,type=!area_effect_cloud] run summon lightning_bolt ~ ~ ~

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=elecbullet] at @s unless block ~ ~ ~ air run summon lightning_bolt ~ ~ ~
execute as @s[type=armor_stand,tag=elecbullet] at @s unless block ~ ~ ~ air run particle end_rod ^ ^ ^-1 1 4 1 0.1 40
execute as @s[type=armor_stand,tag=elecbullet] at @s unless block ~ ~ ~ air run kill @s
