#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |   DMZCraft MCFuntion file for Legendary Items for MC v1.13   |
#      :--------------------------------------------------------------:
#
#
# Wand of Fire - shoots a fireball that catches nearby targets on fire
#
# Not Enough Mana
execute as @s[scores={DMZWandUse=1..,Mana=..1}] at @s run playsound minecraft:entity.egg.throw master @a[distance=..20] ~ ~ ~ 1 0.25 1
scoreboard players reset @s[scores={DMZWandUse=1..,Mana=..1}] DMZWandUse

#projectile spawn
execute as @s[scores={DMZWandUse=1..,Mana=2..}] at @s run summon armor_stand ~ ~ ~ {NoGravity:1b,Small:0b,Marker:1b,Invisible:1b,PersistenceRequired:0b,Tags:["firebullet", "CItemEntity"]}

#teleport to user, teleport slightly in front of user + teleport to eye level
execute as @s[scores={DMZWandUse=1..}] at @s run tp @e[type=armor_stand,tag=firebullet,distance=0..2] @s
execute as @s[scores={DMZWandUse=1..}] at @s anchored eyes run tp @e[type=armor_stand,tag=firebullet,distance=0..2] ^ ^ ^5.5

#sound
execute as @s[scores={DMZWandUse=1..,Mana=2..}] at @s run playsound entity.blaze.shoot master @a[distance=0..20] ~ ~ ~ 0.7 0.8 0.7

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=2..}] Mana 2

#dont take Mana while firing
scoreboard players set @s[scores={DMZWandUse=1..}] Repl 0

#take Mana
scoreboard players remove @s[scores={DMZWandUse=1..,Mana=2..}] Mana 2

#reset
scoreboard players reset @s[scores={DMZWandUse=1..}] DMZWandUse

# Entity command

#particle
execute as @s[type=armor_stand,tag=firebullet] at @s run particle flame ~ ~ ~ 0.8 0.8 0.8 0.01 3
execute as @s[type=armor_stand,tag=firebullet] at @s run particle dripping_lava ~ ~ ~ 0.2 0.2 0.2 0.0 5

#movement
execute as @s[type=armor_stand,tag=firebullet] at @s run tp @s ^ ^ ^1
execute as @s[type=armor_stand,tag=firebullet] at @s run teleport @s ~ ~ ~ ~ ~0.2

#effects
execute as @s[type=armor_stand,tag=firebullet] at @s at @e[tag=!firebullet,distance=0..5,type=!item,type=!experience_orb,type=!item_frame,type=!armor_stand] run setblock ~ ~ ~ fire keep
execute as @s[type=armor_stand,tag=firebullet] at @s run data merge entity @e[distance=0..5,limit=1,nbt=!{Fire:250},type=!item,type=!experience_orb,type=!item_frame,type=!armor_stand,type=!arrow,type=!egg,type=!snowball,type=!potion,type=!leash_knot,type=!painting,type=!llama_spit,type=!area_effect_cloud,type=!trident,type=!painting,type=!llama_spit,type=!area_effect_cloud] {Fire:250}

#kill when in block and spawn particles
execute as @s[type=armor_stand,tag=firebullet] at @s unless block ~ ~ ~ air run particle lava ^ ^ ^-1 1 1 1 0.1 20
execute as @s[type=armor_stand,tag=firebullet] at @s unless block ~ ~ ~ air run kill @s
