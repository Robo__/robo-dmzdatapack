#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |     DMZCraft MCFuntion file for custom armor set checks      |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit these to   |
#      |   @s where possible. This file fires only once criteria met. |
#      |   Many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
# 
# Myth set - grants haste 
effect give @s[nbt={Inventory:[{Slot:100b,tag:{Myth:1b}}]},nbt={Inventory:[{Slot:101b,tag:{Myth:1b}}]},nbt={Inventory:[{Slot:102b,tag:{Myth:1b}}]},nbt={Inventory:[{Slot:103b,tag:{Myth:1b}}]}] haste 1 1 true
