#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |         DMZCraft MCFuntion file for applying glimmers        |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Arcane
execute as @s[type=item,nbt={Item:{tag:{GArcane:1b}}}] at @s run particle enchant ~ ~0.5 ~ 0.1 0.2 0.1 0.2 1
# Ash
execute as @s[type=item,nbt={Item:{tag:{GAsh:1b}}}] at @s run particle mycelium ~ ~0.5 ~ 0.2 0.2 0.2 0.01 1
# Bubble
execute as @s[type=item,nbt={Item:{tag:{GBubble:1b}}}] at @s run particle bubble_pop ~ ~0.5 ~ 0.15 0.2 0.15 0.0 1
# Conduit Covered
execute as @s[type=item,nbt={Item:{tag:{GNautilus:1b}}}] at @s run particle nautilus ~ ~0.5 ~ 0.1 0.2 0.1 0.2 1
# Confetti
execute as @s[type=item,nbt={Item:{tag:{GParty:1b}}}] at @s run particle entity_effect ~ ~0.35 ~ 0.15 0.0 0.15 100 1 normal
# Critical
execute as @s[type=item,nbt={Item:{tag:{GCrit:1b}}}] at @s run particle enchanted_hit ~ ~0.5 ~ 0.1 0.15 0.1 0.0 1
# Deadshot
execute as @s[type=item,nbt={Item:{tag:{GArrow:1b}}}] at @s run particle crit ~ ~0.5 ~ 0.1 0.15 0.1 0.0 1
# Diamond Encrusted
execute as @s[type=item,nbt={Item:{tag:{GMoney1:1b}}}] at @s run particle block diamond_block ~ ~0.5 ~ 0.15 0.2 0.15 0.01 1
# Dragon Energy
execute as @s[type=item,nbt={Item:{tag:{GDragon:1b}}}] at @s run particle dragon_breath ~ ~0.5 ~ 0.1 0.1 0.1 0.0 1
# Emerald Infused
execute as @s[type=item,nbt={Item:{tag:{GMoney2:1b}}}] at @s run particle block emerald_block ~ ~0.5 ~ 0.15 0.2 0.15 0.01 1
# Extraplanar
execute as @s[type=item,nbt={Item:{tag:{GEnd:1b}}}] at @s run particle portal ~ ~0.25 ~ 0.1 0.0 0.1 0.2 1
# Fizzy
execute as @s[type=item,nbt={Item:{tag:{GFizzle:1b}}}] at @s run particle fishing ~ ~0.5 ~ 0.15 0.2 0.15 0.0 1
# Heavenly
execute as @s[type=item,nbt={Item:{tag:{GHeaven:1b}}}] at @s run particle instant_effect ~ ~0.35 ~ 0.25 0.0 0.25 100 1 normal
# Inferno
execute as @s[type=item,nbt={Item:{tag:{GFire:1b}}}] at @s run particle flame ~ ~0.5 ~ 0.1 0.2 0.1 0.01 1
# Iron Curtain
execute as @s[type=item,nbt={Item:{tag:{GMoney3:1b}}}] at @s run particle block iron_block ~ ~0.5 ~ 0.15 0.2 0.15 0.01 1
# Lapis Love
execute as @s[type=item,nbt={Item:{tag:{GMoney4:1b}}}] at @s run particle block lapis_block ~ ~0.5 ~ 0.15 0.2 0.15 0.01 1
# Midas Touch
execute as @s[type=item,nbt={Item:{tag:{GMoney:1b}}}] at @s run particle block gold_block ~ ~0.5 ~ 0.15 0.2 0.15 0.01 1
# Redstone Ready
execute as @s[type=item,nbt={Item:{tag:{GRedstone:1b}}}] at @s run particle dust 1 0 0 1 ~ ~0.5 ~ 0.1 0.2 0.1 0.0 1
# Slimed
execute as @s[type=item,nbt={Item:{tag:{GSlime:1b}}}] at @s run particle item_slime ~ ~0.5 ~ 0.1 0.2 0.1 0.0 1
# Smoke Screen
execute as @s[type=item,nbt={Item:{tag:{GSmoke:1b}}}] at @s run particle smoke ~ ~0.5 ~ 0.1 0.2 0.1 0.01 1
# Vapor
execute as @s[type=item,nbt={Item:{tag:{GVapor:1b}}}] at @s run particle dolphin ~ ~0.5 ~ 0.2 0.2 0.2 0.01 1
