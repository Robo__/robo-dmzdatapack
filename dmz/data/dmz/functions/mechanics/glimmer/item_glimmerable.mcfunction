# Apply Glimmers - Only triggers on entities within 30 of a player since players cannot view item entities past that point
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:diamond_helmet"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:diamond_chestplate"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:diamond_legging"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:diamond_boots"}}] {Item:{tag:{Glimmerable:1b}}}
#
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:gold_helmet"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:gold_chestplate"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:gold_legging"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:gold_boots"}}] {Item:{tag:{Glimmerable:1b}}}
#
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:iron_helmet"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:iron_chestplate"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:iron_legging"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:iron_boots"}}] {Item:{tag:{Glimmerable:1b}}}
#
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:chainmail_helmet"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:chainmail_chestplate"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:chainmail_legging"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:chainmail_boots"}}] {Item:{tag:{Glimmerable:1b}}}
#
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:leather_helmet"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:leather_chestplate"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:leather_legging"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:leather_boots"}}] {Item:{tag:{Glimmerable:1b}}}
#
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:turtle_helmet"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:elytra"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:carved_pumpkin"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:skeleton_skull"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:zombie_skull"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:player_skull"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:creeper_skull"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:wither_skeleton_skull"}}] {Item:{tag:{Glimmerable:1b}}}
execute as @s run data merge entity @e[distance=..2,sort=nearest,limit=1,nbt={Item:{id:"minecraft:dragon_skull"}}] {Item:{tag:{Glimmerable:1b}}}
