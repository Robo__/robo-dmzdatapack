#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |         DMZCraft MCFuntion file for applying glimmers        |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Arcane
execute as @s[nbt={Inventory:[{tag:{GArcane:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/arcane
# Ash
execute as @s[nbt={Inventory:[{tag:{GAsh:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/ash
# Bubble
execute as @s[nbt={Inventory:[{tag:{GBubble:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/bubble
# Conduit Covered
execute as @s[nbt={Inventory:[{tag:{GNautilus:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/conduit_covered
# Confetti
execute as @s[nbt={Inventory:[{tag:{GParty:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/confetti
# Critical
execute as @s[nbt={Inventory:[{tag:{GCrit:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/critical
# Deadshot
execute as @s[nbt={Inventory:[{tag:{GArrow:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/deadshot
# Diamond Encrusted
execute as @s[nbt={Inventory:[{tag:{GMoney1:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/diamond_encrusted
# Dragon Energy
execute as @s[nbt={Inventory:[{tag:{GDragon:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/dragon_energy
# Emerald Infused
execute as @s[nbt={Inventory:[{tag:{GMoney2:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/emerald_infused
# Extraplanar
execute as @s[nbt={Inventory:[{tag:{GEnd:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/extraplanar
# Fizzy
execute as @s[nbt={Inventory:[{tag:{GFizzle:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/fizzy
# Heavenly
execute as @s[nbt={Inventory:[{tag:{GHeaven:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/heavenly
# Inferno
execute as @s[nbt={Inventory:[{tag:{GFire:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/inferno
# Iron Curtain
execute as @s[nbt={Inventory:[{tag:{GMoney3:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/iron_curtain
# Lapis Love
execute as @s[nbt={Inventory:[{tag:{GMoney4:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/lapis_love
# Midas Touch
execute as @s[nbt={Inventory:[{tag:{GMoney:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/midas_touch
# Redstone Ready
execute as @s[nbt={Inventory:[{tag:{GRedstone:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/redstone_ready
# Slimed
execute as @s[nbt={Inventory:[{tag:{GSlime:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/slimed
# Smoke Screen
execute as @s[nbt={Inventory:[{tag:{GSmoke:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/smoke_screen
# Vapor
execute as @s[nbt={Inventory:[{tag:{GVapor:1b}}]}] at @s run function dmz:mechanics/glimmer/effects/vapor
