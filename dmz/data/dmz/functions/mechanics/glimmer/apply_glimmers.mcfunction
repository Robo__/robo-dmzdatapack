#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |         DMZCraft MCFuntion file for applying glimmers        |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
# Arcane
execute as @s[nbt={Item:{tag:{GArcane:1b}}}] at @s run function dmz:mechanics/glimmer/apply/arcane
# Ash
execute as @s[nbt={Item:{tag:{GAsh:1b}}}] at @s run function dmz:mechanics/glimmer/apply/ash
# Bubble
execute as @s[nbt={Item:{tag:{GBubble:1b}}}] at @s run function dmz:mechanics/glimmer/apply/bubble
# Conduit Covered
execute as @s[nbt={Item:{tag:{GNautilus:1b}}}] at @s run function dmz:mechanics/glimmer/apply/conduit_covered
# Confetti
execute as @s[nbt={Item:{tag:{GParty:1b}}}] at @s run function dmz:mechanics/glimmer/apply/confetti
# Critical
execute as @s[nbt={Item:{tag:{GCrit:1b}}}] at @s run function dmz:mechanics/glimmer/apply/critical
# Deadshot
execute as @s[nbt={Item:{tag:{GArrow:1b}}}] at @s run function dmz:mechanics/glimmer/apply/deadshot
# Diamond Encrusted
execute as @s[nbt={Item:{tag:{GMoney1:1b}}}] at @s run function dmz:mechanics/glimmer/apply/diamond_encrusted
# Dragon Energy
execute as @s[nbt={Item:{tag:{GDragon:1b}}}] at @s run function dmz:mechanics/glimmer/apply/dragon_energy
# Emerald Infused
execute as @s[nbt={Item:{tag:{GMoney2:1b}}}] at @s run function dmz:mechanics/glimmer/apply/emerald_infused
# Extraplanar
execute as @s[nbt={Item:{tag:{GEnd:1b}}}] at @s run function dmz:mechanics/glimmer/apply/extraplanar
# Fizzy
execute as @s[nbt={Item:{tag:{GFizzle:1b}}}] at @s run function dmz:mechanics/glimmer/apply/fizzy
# Heavenly
execute as @s[nbt={Item:{tag:{GHeaven:1b}}}] at @s run function dmz:mechanics/glimmer/apply/heavenly
# Inferno
execute as @s[nbt={Item:{tag:{GFire:1b}}}] at @s run function dmz:mechanics/glimmer/apply/inferno
# Iron Curtain
execute as @s[nbt={Item:{tag:{GMoney3:1b}}}] at @s run function dmz:mechanics/glimmer/apply/iron_curtain
# Lapis Love
execute as @s[nbt={Item:{tag:{GMoney4:1b}}}] at @s run function dmz:mechanics/glimmer/apply/lapis_love
# Midas Touch
execute as @s[nbt={Item:{tag:{GMoney:1b}}}] at @s run function dmz:mechanics/glimmer/apply/midas_touch
# Redstone Ready
execute as @s[nbt={Item:{tag:{GRedstone:1b}}}] at @s run function dmz:mechanics/glimmer/apply/redstone_ready
# Slimed
execute as @s[nbt={Item:{tag:{GSlime:1b}}}] at @s run function dmz:mechanics/glimmer/apply/slimed
# Smoke Screen
execute as @s[nbt={Item:{tag:{GSmoke:1b}}}] at @s run function dmz:mechanics/glimmer/apply/smoke_screen
# Vapor
execute as @s[nbt={Item:{tag:{GVapor:1b}}}] at @s run function dmz:mechanics/glimmer/apply/vapor
