#Redstone Ready

execute as @s at @s run execute as @e[type=item,sort=nearest,nbt=!{Item:{tag:{ApplyGlimmer:1b}}},distance=0..1,nbt={Item:{tag:{GlimmerItem:1b}}},limit=1] run data merge entity @s {Item:{tag:{HasGlimmer:1b}}}

execute as @s at @s run execute as @e[type=item,sort=nearest,nbt=!{Item:{tag:{ApplyGlimmer:1b}}},distance=0..1,nbt=!{Item:{tag:{GlimmerItem:1b}}},limit=1] at @s run playsound entity.blaze.shoot master @a[distance=0..5] ~ ~ ~ 0.3 1.4 0.3

execute as @s at @s run execute as @e[type=item,sort=nearest,nbt=!{Item:{tag:{ApplyGlimmer:1b}}},distance=0..1,nbt=!{Item:{tag:{GlimmerItem:1b}}},limit=1] at @s run data merge entity @s {Item:{tag:{GRedstone:1b}}}

execute as @s at @s run execute as @e[type=item,sort=nearest,nbt=!{Item:{tag:{ApplyGlimmer:1b}}},distance=0..1,nbt=!{Item:{tag:{GlimmerItem:1b}}},limit=1] at @s run data merge entity @s {Item:{tag:{GlimmerItem:1b}}}

execute as @s at @s run execute as @e[type=item,sort=nearest,nbt=!{Item:{tag:{ApplyGlimmer:1b}}},distance=0..1,nbt={Item:{tag:{GRedstone:1b}}},nbt=!{Item:{tag:{HasGlimmer:1b}}}] as @e[type=item,sort=nearest,nbt={Item:{tag:{ApplyGlimmer:1b}}},distance=0..1,nbt={Item:{tag:{GRedstone:1b}}},nbt=!{Item:{tag:{HasGlimmer:1b}}},limit=1] run kill @s
