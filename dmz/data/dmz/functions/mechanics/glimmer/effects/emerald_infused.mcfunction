#Emerald Infused

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GMoney2:1b}}]}] at @s anchored eyes run particle block emerald_block ^ ^1 ^ 0.15 0 0.15 0.01 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GMoney2:1b}}]}] at @s run particle block emerald_block ~ ~1.1 ~ 0.2 0.1 0.2 0.01 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GMoney2:1b}}]}] at @s run particle block emerald_block ~ ~0.5 ~ 0.15 0.2 0.15 0.01 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GMoney2:1b}}]}] at @s anchored feet run particle block emerald_block ^ ^0.1 ^ 0.15 0.1 0.15 0.1 1

