#Dragon Energy

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GDragon:1b}}]}] at @s anchored eyes run particle dragon_breath ^ ^0.75 ^ 0.1 0.1 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GDragon:1b}}]}] at @s run particle dragon_breath ~ ~1 ~ 0.2 0.15 0.2 0.0 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GDragon:1b}}]}] at @s run particle dragon_breath ~ ~0.5 ~ 0.1 0.2 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GDragon:1b}}]}] at @s anchored feet run particle dragon_breath ^ ^ ^ 0.2 0.0 0.2 0.0 1

