#Arcane

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GArcane:1b}}]}] at @s anchored eyes run particle enchant ^ ^1.5 ^ 0.2 0.0 0.2 0.4 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GArcane:1b}}]}] at @s run particle enchant ~ ~1 ~ 0.2 0.1 0.2 0.2 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GArcane:1b}}]}] at @s run particle enchant ~ ~0.5 ~ 0.1 0.2 0.1 0.2 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GArcane:1b}}]}] at @s anchored feet run particle enchant ^ ^ ^ 0.2 0.0 0.2 0.2 1

