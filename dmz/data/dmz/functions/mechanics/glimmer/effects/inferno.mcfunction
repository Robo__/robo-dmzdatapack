#Inferno

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GFire:1b}}]}] at @s anchored eyes run particle flame ^ ^0.65 ^ 0.1 0.15 0.1 0.01 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GFire:1b}}]}] at @s run particle flame ~ ~1 ~ 0.2 0.1 0.2 0.01 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GFire:1b}}]}] at @s run particle flame ~ ~0.5 ~ 0.1 0.2 0.1 0.01 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GFire:1b}}]}] at @s anchored feet run particle flame ^ ^0.1 ^ 0.15 0.1 0.15 0.01 1

