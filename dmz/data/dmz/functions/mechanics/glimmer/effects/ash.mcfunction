#Ash

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GAsh:1b}}]}] at @s anchored eyes run particle mycelium ^ ^0.65 ^ 0.2 0.2 0.2 0.01 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GAsh:1b}}]}] at @s run particle mycelium ~ ~1 ~ 0.3 0.15 0.3 0.01 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GAsh:1b}}]}] at @s run particle mycelium ~ ~0.5 ~ 0.2 0.2 0.2 0.01 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GAsh:1b}}]}] at @s anchored feet run particle mycelium ^ ^0.1 ^ 0.25 0.15 0.25 0.01 1

