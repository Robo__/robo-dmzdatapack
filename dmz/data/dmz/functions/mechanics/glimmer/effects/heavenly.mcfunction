#Heavenly

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GHeaven:1b}}]}] at @s anchored eyes run particle instant_effect ^ ^0.75 ^ 0.15 0.2 0.15 100 1 normal

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GHeaven:1b}}]}] at @s run particle instant_effect ~ ~0.625 ~ 0.35 0 0.35 100 1 normal

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GHeaven:1b}}]}] at @s run particle instant_effect ~ ~0.3 ~ 0.25 0.0 0.25 100 1 normal

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GHeaven:1b}}]}] at @s anchored feet run particle instant_effect ^ ^ ^ 0.25 0.1 0.25 100 1 normal

