#Bubbler

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GBubble:1b}}]}] at @s anchored eyes run particle bubble_pop ^ ^0.75 ^ 0.15 0.15 0.15 0.0 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GBubble:1b}}]}] at @s run particle bubble_pop ~ ~1.25 ~ 0.25 0.05 0.25 0.0 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GBubble:1b}}]}] at @s run particle bubble_pop ~ ~0.5 ~ 0.15 0.2 0.15 0.0 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GBubble:1b}}]}] at @s anchored feet run particle bubble_pop ^ ^0.1 ^ 0.2 0.125 0.2 0.0 1

