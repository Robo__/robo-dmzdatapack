#Smoke Screen

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GSmoke:1b}}]}] at @s anchored eyes run particle smoke ^ ^0.5 ^ 0.1 0.05 0.1 0.01 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GSmoke:1b}}]}] at @s run particle smoke ~ ~0.85 ~ 0.2 0.05 0.2 0.01 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GSmoke:1b}}]}] at @s run particle smoke ~ ~0.5 ~ 0.1 0.2 0.1 0.01 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GSmoke:1b}}]}] at @s anchored feet run particle smoke ^ ^0.1 ^ 0.15 0.1 0.15 0.01 1

