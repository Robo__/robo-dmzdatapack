#Conduit Covered

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GNautilus:1b}}]}] at @s anchored eyes run particle nautilus ^ ^1.5 ^ 0.2 0.0 0.2 0.4 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GNautilus:1b}}]}] at @s run particle nautilus ~ ~1 ~ 0.2 0.15 0.2 0.2 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GNautilus:1b}}]}] at @s run particle nautilus ~ ~0.5 ~ 0.1 0.2 0.1 0.2 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GNautilus:1b}}]}] at @s anchored feet run particle nautilus ^ ^ ^ 0.2 0.0 0.2 0.2 1

