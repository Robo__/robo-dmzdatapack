#Redstone Ready

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GRedstone:1b}}]}] at @s anchored eyes run particle dust 1 0 0 1 ^ ^0.55 ^ 0.1 0.1 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GRedstone:1b}}]}] at @s run particle dust 1 0 0 1 ~ ~1 ~ 0.2 0.15 0.2 0.0 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GRedstone:1b}}]}] at @s run particle dust 1 0 0 1 ~ ~0.5 ~ 0.1 0.2 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GRedstone:1b}}]}] at @s anchored feet run particle dust 1 0 0 1 ^ ^0.1 ^ 0.15 0.1 0.15 0.0 1

