#Critical

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GCrit:1b}}]}] at @s anchored eyes run particle enchanted_hit ^ ^0.65 ^ 0.1 0.2 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GCrit:1b}}]}] at @s run particle enchanted_hit ~ ~1.25 ~ 0.2 0.05 0.2 0.0 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GCrit:1b}}]}] at @s run particle enchanted_hit ~ ~0.5 ~ 0.1 0.15 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GCrit:1b}}]}] at @s anchored feet run particle enchanted_hit ^ ^0.1 ^ 0.15 0.1 0.15 0.0 1

