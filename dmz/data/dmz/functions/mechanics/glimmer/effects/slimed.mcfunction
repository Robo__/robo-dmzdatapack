#Slimed

execute as @s[nbt={Inventory:[{Slot:103b,tag:{GSlime:1b}}]}] at @s anchored eyes run particle item_slime ^ ^0.65 ^ 0.1 0.15 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:102b,tag:{GSlime:1b}}]}] at @s run particle item_slime ~ ~1 ~ 0.2 0.15 0.2 0.0 1

execute as @s[nbt={Inventory:[{Slot:101b,tag:{GSlime:1b}}]}] at @s run particle item_slime ~ ~0.5 ~ 0.1 0.2 0.1 0.0 1

execute as @s[nbt={Inventory:[{Slot:100b,tag:{GSlime:1b}}]}] at @s anchored feet run particle item_slime ^ ^0.1 ^ 0.15 0.1 0.15 0.0 1

