#                        ,*.                                                                                 
#                     .*///////*                                                                              
#                   ,///////////////.                                                                          
#               .*///////%@@@@@#///////*                                                                       
#             ///////(@@@@@@@@@@@@&//////*                                
#             (###&@@@@@@@@@@@@@@@@@@@/***   */////,   /*     */* ////////
#             (###@@@@@@@@@@@@@@@@@@@@/***   */   ///* ///   *//*     *// 
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// //// *///*    *//  
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    /// // //// /*   *//   
#             (###@@@@@@@@@@@@@@@@@@@@/***   */    */* //  //  /*  *//    
#             (###@@@@@@@@@@@@@@@@@@@@/***   *///////  //      /* *///////
#             (####%@@@@@@@@@@@@@@@@%*****                                                                     
#              ########&@@@@@@@@@/*******.                                                                     
#                 *#######%@@#********                                                                         
#                     (#####/*****.                                                                            
#                        *##/*, 
#      .--------------------------------------------------------------.
#      |     DMZCraft MCFuntion file that is executed every tick      |
#      :--------------------------------------------------------------:
#      |  These functions fire 20x a second.  Please limit this to    |
#      |   calling another file to run once a parameter is met. Too   |
#      |   many commands executing this often will cause server lag.  |
#      |  Other functions called from here will run in the same tick. |
#      '--------------------------------------------------------------'
#
#
# Elevator Ascend
execute as @a[scores={ElevatorJump=1..}] at @s if block ~ ~-1 ~ minecraft:lapis_block run function dmz:mechanics/elevators_jump
execute as @a[scores={ElevatorJump=1..}] run scoreboard players reset @s ElevatorJump
#
# Elevator Descend
execute as @a[scores={ElevatorSneak=1..}] at @s if block ~ ~-1 ~ minecraft:lapis_block run function dmz:mechanics/elevators_sneak
execute as @a[scores={ElevatorSneak=1..}] run scoreboard players reset @s ElevatorSneak
execute as @a[scores={ElevatorTicker=10..}] run scoreboard players reset @s ElevatorTicker
execute as @a[scores={ElevatorTicker=1..}] run scoreboard players add @s ElevatorTicker 1
#
# Mana
execute as @a at @s if score @s Mana < @s ManaMax run function dmz:mechanics/mana/mana
execute as @a at @s if score @s Mana > @s ManaMax run function dmz:mechanics/mana/mana
scoreboard players set @a HasMana 0
scoreboard players set @a[scores={ManaMax=0..,Mana=0..}] HasMana 1
scoreboard players set @a[scores={HasMana=0}] ManaMax 20
scoreboard players set @a[scores={HasMana=0}] Mana 0
# Armor that changes ManaMax
scoreboard players set @a[nbt={Inventory:[{Slot:100b,tag:{Arcane:1b}}]},nbt={Inventory:[{Slot:101b,tag:{Arcane:1b}}]},nbt={Inventory:[{Slot:102b,tag:{Arcane:1b}}]},nbt={Inventory:[{Slot:103b,tag:{Arcane:1b}}]},scores={ManaMax=20..}] ManaMax 30
scoreboard players set @a[nbt=!{Inventory:[{Slot:100b,tag:{Arcane:1b}}]},scores={ManaMax=21..}] ManaMax 20
scoreboard players set @a[nbt=!{Inventory:[{Slot:101b,tag:{Arcane:1b}}]},scores={ManaMax=21..}] ManaMax 20
scoreboard players set @a[nbt=!{Inventory:[{Slot:102b,tag:{Arcane:1b}}]},scores={ManaMax=21..}] ManaMax 20
scoreboard players set @a[nbt=!{Inventory:[{Slot:103b,tag:{Arcane:1b}}]},scores={ManaMax=21..}] ManaMax 20
#
# Apply Glimmers - Only triggers on entities within 30 of a player since players cannot view item entities past that point
execute at @a as @e[type=item,nbt={Item:{tag:{ApplyGlimmer:1b}}},distance=..10] at @s run function dmz:mechanics/glimmer/item_glimmerable
#
execute at @a as @e[type=item,nbt={Item:{tag:{ApplyGlimmer:1b}}},distance=..8] at @s run function dmz:mechanics/glimmer/apply_glimmers
#
# Show glimmer effects - Only triggers on entities within 30 of a player since players cannot view item entities past that point - Also limits commands run severely
execute at @a as @e[type=item,nbt={Item:{tag:{GlimmerItem:1b}}},distance=..30] at @s run function dmz:mechanics/glimmer/entity_glimmers
execute as @a[nbt={Inventory:[{Slot:103b,tag:{GlimmerItem:1b}}]}] at @s run function dmz:mechanics/glimmer/glimmers
execute as @a[nbt={Inventory:[{Slot:102b,tag:{GlimmerItem:1b}}]}] at @s run function dmz:mechanics/glimmer/glimmers
execute as @a[nbt={Inventory:[{Slot:101b,tag:{GlimmerItem:1b}}]}] at @s run function dmz:mechanics/glimmer/glimmers
execute as @a[nbt={Inventory:[{Slot:100b,tag:{GlimmerItem:1b}}]}] at @s run function dmz:mechanics/glimmer/glimmers
